CREATE DATABASE quizbon
  WITH OWNER = quizbon
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_US.UTF-8'
       LC_CTYPE = 'en_US.UTF-8'
       CONNECTION LIMIT = -1;

CREATE SEQUENCE answer_answerid_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE answer_answerid_seq
  OWNER TO quizbon;

CREATE SEQUENCE question_questionid_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE question_questionid_seq
  OWNER TO quizbon;
  
 CREATE SEQUENCE quiz_result_quizresultid_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE quiz_result_quizresultid_seq
  OWNER TO quizbon;
  
  CREATE SEQUENCE user_quiz_userquizid_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE user_quiz_userquizid_seq
  OWNER TO quizbon;
  
  CREATE SEQUENCE user_test_statistic_userteststatisticid_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE user_test_statistic_userteststatisticid_seq
  OWNER TO quizbon;
  
  CREATE SEQUENCE users_userid_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE users_userid_seq
  OWNER TO quizbon;
       
CREATE TABLE users
(
  user_id integer NOT NULL DEFAULT nextval('users_userid_seq'::regclass),
  first_name character varying(50) NOT NULL,
  last_name character varying(50) NOT NULL,
  phone character varying(50) NOT NULL,
  email character varying(50) NOT NULL,
  skype character varying(50),
  active boolean NOT NULL DEFAULT true,
  creation_date timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT users_pkey PRIMARY KEY (user_id)
);

CREATE TABLE user_test_statistic
(
  user_test_statistic_id bigint NOT NULL DEFAULT nextval('user_test_statistic_userteststatisticid_seq'::regclass),
  user_id bigint NOT NULL,
  result integer,
  test_max integer,
  test_start_time timestamp without time zone,
  test_end_time timestamp without time zone,
  CONSTRAINT user_test_statistic_pkey PRIMARY KEY (user_test_statistic_id),
  CONSTRAINT user_test_statistic_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES users (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE question
(
  question_id integer NOT NULL DEFAULT nextval('question_questionid_seq'::regclass),
  text character varying(700),
  active boolean NOT NULL DEFAULT true,
  CONSTRAINT "Question_pkey" PRIMARY KEY (question_id)
);

CREATE TABLE answer
(
  answer_id integer NOT NULL DEFAULT nextval('answer_answerid_seq'::regclass),
  text character varying(700),
  question_id bigint,
  correct boolean,
  active boolean DEFAULT true,
  CONSTRAINT "Answer_pkey" PRIMARY KEY (answer_id),
  CONSTRAINT "Answer_id_fkey" FOREIGN KEY (question_id)
      REFERENCES question (question_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
); 

CREATE TABLE user_question
(
  user_question_id integer NOT NULL DEFAULT nextval('user_quiz_userquizid_seq'::regclass),
  user_id bigint NOT NULL,
  question_id bigint NOT NULL,
  CONSTRAINT "UserQuizId_pkey" PRIMARY KEY (user_question_id),
  CONSTRAINT "UserId_fkey" FOREIGN KEY (user_id)
      REFERENCES users (user_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE user_answer
(
  user_answer_id integer NOT NULL DEFAULT nextval('quiz_result_quizresultid_seq'::regclass),
  user_question_id bigint NOT NULL,
  answer_id bigint NOT NULL,
  checked boolean,
  CONSTRAINT "QuizResultId_pkey" PRIMARY KEY (user_answer_id),
  CONSTRAINT "AnswerId_fkey" FOREIGN KEY (answer_id)
      REFERENCES answer (answer_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "UserQuizId_fkey" FOREIGN KEY (user_question_id)
      REFERENCES user_question (user_question_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

