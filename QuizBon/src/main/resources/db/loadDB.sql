TRUNCATE question, answer,user_answer, user_question, users;
TRUNCATE question RESTART IDENTITY CASCADE;
insert into question (text) values('What is the meaning of the return data type void?');

insert into answer(text, question_id, correct) values('An empty memory space is returned so that the developers can utilize it.', 1, false);
insert into answer(text, question_id, correct) values('void returns no data type.', 1, true);
insert into answer(text, question_id, correct) values('void is not supported in Java', 1, false);
insert into answer(text, question_id, correct) values('None of the above', 1, false);

insert into question (text) values('The object of DataInputStream is used to');

insert into answer(text, question_id, correct) values('To covert binary stream into character stream', 2, true);
insert into answer(text, question_id, correct) values('To covert character stream into binary stream', 2, false);
insert into answer(text, question_id, correct) values('To write data onto output object', 2, false);
insert into answer(text, question_id, correct) values('All of the above', 2, false);

insert into question (text) values('When a thread terminates its processing, into what state that thread enters?');

insert into answer(text, question_id, correct) values('Running state', 3, false);
insert into answer(text, question_id, correct) values('Waiting state', 3, false);
insert into answer(text, question_id, correct) values('Dead state', 3, true);
insert into answer(text, question_id, correct) values('Beginning state', 3, false);

insert into question (text) values('What is the data type for the number 9.6352?');

insert into answer(text, question_id, correct) values('float', 4, false);
insert into answer(text, question_id, correct) values('double', 4, true);
insert into answer(text, question_id, correct) values('Float', 4, false);
insert into answer(text, question_id, correct) values('Double', 4, false);

insert into question (text) values('Which of the following statements declare class Sample to belong to the payroll.admindept package?');

insert into answer(text, question_id, correct) values('package payroll; package admindept;', 5, false);
insert into answer(text, question_id, correct) values('import payroll.*;', 5, false);
insert into answer(text, question_id, correct) values('package payroll.admindept.Sample;', 5, false);
insert into answer(text, question_id, correct) values('import payroll.admindept.*;', 5, false);
insert into answer(text, question_id, correct) values('package payroll.admindept;', 5, true);

insert into question (text) values('Assume that the value 3929.92 is of type ‘float’. How to assign this value after declaring the variable ‘interest’ of type float?');

insert into answer(text, question_id, correct) values('interest = 3929.92', 6, false);
insert into answer(text, question_id, correct) values('interest = (Float)3929.92', 6, false);
insert into answer(text, question_id, correct) values('interest = 3929.92 (float)', 6, false);
insert into answer(text, question_id, correct) values('interest = 3929.92f', 6, true);

insert into question (text) values('Which of these statements is true?');

insert into answer(text, question_id, correct) values('LinkedList extends List', 7, false);
insert into answer(text, question_id, correct) values('HashSet extends AbstractSet', 7, true);
insert into answer(text, question_id, correct) values('AbstractSet extends Set', 7, false);
insert into answer(text, question_id, correct) values('WeakHashMap extends HashMap', 7, false);

insert into question (text) values('Which of the following is not a method of the Thread class.');

insert into answer(text, question_id, correct) values('public void run()', 8, false);
insert into answer(text, question_id, correct) values('public void start()', 8, false);
insert into answer(text, question_id, correct) values('public void exit()', 8, true);
insert into answer(text, question_id, correct) values('public final int getPriority()', 8, false);

insert into question (text) values('The class java.lang.Exception is');

insert into answer(text, question_id, correct) values('protected', 9, false);
insert into answer(text, question_id, correct) values('extends Throwable', 9, true);
insert into answer(text, question_id, correct) values('implements Throwable', 9, false);
insert into answer(text, question_id, correct) values('serializable', 9, false);

insert into question (text) values('Which statement is true?');

insert into answer(text, question_id, correct) values('HashTable is a sub class of Dictionary', 10, true);
insert into answer(text, question_id, correct) values('ArrayList is a sub class of Vector', 10, false);
insert into answer(text, question_id, correct) values('LinkedList is a subclass of ArrayList', 10, false);
insert into answer(text, question_id, correct) values('Vector is a subclass of Stack', 10, false);

insert into question (text) values('What is an aggregate object?');

insert into answer(text, question_id, correct) values('An object with only primitive attributes', 11, false);
insert into answer(text, question_id, correct) values('An instance of a class which has only static methods', 11, false);
insert into answer(text, question_id, correct) values('An instance which has other objects', 11, true);
insert into answer(text, question_id, correct) values('None of the above', 11, false);

insert into question (text) values('How many numeric data types are supported in Java?');

insert into answer(text, question_id, correct) values('8', 12, false);
insert into answer(text, question_id, correct) values('4', 12, false);
insert into answer(text, question_id, correct) values('6', 12, true);
insert into answer(text, question_id, correct) values('2', 12, false);

insert into question (text) values('What are the two parts of a value of type double?');

insert into answer(text, question_id, correct) values('Significant Digits, Exponent', 13, true);
insert into answer(text, question_id, correct) values('Length, Denominator', 13, false);
insert into answer(text, question_id, correct) values('Mode, Numerator', 13, false);

insert into question (text) values('A class can have many methods with the same name as long as the number of parameters or type of parameters is different. This OOP concept is known as');

insert into answer(text, question_id, correct) values('Method Invocating', 14, false);
insert into answer(text, question_id, correct) values('Method Overriding', 14, false);
insert into answer(text, question_id, correct) values('Method Labeling', 14, false);
insert into answer(text, question_id, correct) values('Method Overloading', 14, true);

insert into question (text) values('Which of the following statements about the Java language is true?');

insert into answer(text, question_id, correct) values('Both procedural and OOP are supported in Java.', 15, true);
insert into answer(text, question_id, correct) values('Java supports only procedural approach towards programming.', 15, false);
insert into answer(text, question_id, correct) values('Java supports only OOP approach.', 15, false);
insert into answer(text, question_id, correct) values('None of the above.', 15, false);

insert into question (text) values('Which methods can access to private attributes of a class?');

insert into answer(text, question_id, correct) values('Only Static methods of the same class', 16, false);
insert into answer(text, question_id, correct) values('Only instances of the same class', 16, false);
insert into answer(text, question_id, correct) values('Only methods those defined in the same class', 16, true);
insert into answer(text, question_id, correct) values('Only classes available in the same package.', 16, false);

insert into question (text) values('DataInputStream is an example of');

insert into answer(text, question_id, correct) values('Output stream', 17, false);
insert into answer(text, question_id, correct) values('I/O stream', 17, false);
insert into answer(text, question_id, correct) values('Filtered stream', 17, true);
insert into answer(text, question_id, correct) values('File stream', 17, false);

insert into question (text) values('A lower precision can be assigned to a higher precision value in Java. For example a byte type data can be assigned to int type.');

insert into answer(text, question_id, correct) values('True', 18, true);
insert into answer(text, question_id, correct) values('False', 18, false);

insert into question (text) values('Which of the following statements is true?');

insert into answer(text, question_id, correct) values('An exception can be thrown by throw keyword explicitly.', 19, true);
insert into answer(text, question_id, correct) values('An exception can be thrown by throws keyword explicitly', 19, false);

insert into question (text) values('Which of the following is not a return type?');

insert into answer(text, question_id, correct) values('boolean', 20, false);
insert into answer(text, question_id, correct) values('void', 20, false);
insert into answer(text, question_id, correct) values('public', 20, true);
insert into answer(text, question_id, correct) values('Button', 20, false);

insert into question (text) values('Consider the following code snippet. What will be assigned to the variable fourthChar, if the code is executed?<br><br>String str = new String(“Java”);<br>char 

fourthChar = str.charAt(4);');

insert into answer(text, question_id, correct) values('a', 21, false);
insert into answer(text, question_id, correct) values('v', 21, false);
insert into answer(text, question_id, correct) values('throws StringIndexOutofBoundsException.', 21, true);
insert into answer(text, question_id, correct) values('null character', 21, false);

insert into question (text) values('Which of the following is synchronized?');

insert into answer(text, question_id, correct) values('Set', 22, false);
insert into answer(text, question_id, correct) values('LinkedList', 22, false);
insert into answer(text, question_id, correct) values('Vector', 22, true);
insert into answer(text, question_id, correct) values('WeakHashMap', 22, false);

insert into question (text) values('Which of the methods should be implemented if any class implements the Runnable interface?');

insert into answer(text, question_id, correct) values('start()', 23, false);
insert into answer(text, question_id, correct) values('run()', 23, true);
insert into answer(text, question_id, correct) values('wait()', 23, false);
insert into answer(text, question_id, correct) values('notify() and notifyAll()', 23, false);

insert into question (text) values('Which of the following statements is false about objects?');

insert into answer(text, question_id, correct) values('An instance of a class is an object', 24, false);
insert into answer(text, question_id, correct) values('Objects can access both static and instance data', 24, false);
insert into answer(text, question_id, correct) values('Object is the super class of all other classes', 24, false);
insert into answer(text, question_id, correct) values('Objects do not permit encapsulation', 24, true);

insert into question (text) values('All the wrapper classes (Integer, Boolean, Float, Short, Long, Double and Character) in java');

insert into answer(text, question_id, correct) values('are private', 25, false);
insert into answer(text, question_id, correct) values('are serializable', 25, false);
insert into answer(text, question_id, correct) values('are immutatable', 25, false);
insert into answer(text, question_id, correct) values('are final', 25, true);

insert into question (text) values('Select all the true statements from the following');

insert into answer(text, question_id, correct) values('AbstractSet extends AbstractCollection', 26, false);
insert into answer(text, question_id, correct) values('AbstractList extends AbstractCollection', 26, false);
insert into answer(text, question_id, correct) values('HashSet extends AbstractSet', 26, false);
insert into answer(text, question_id, correct) values('Vector extends AbstractList', 26, false);
insert into answer(text, question_id, correct) values('All of the above', 26, true);

insert into question (text) values('If result = 2 + 3 * 5, what is the value and type of ‘result’ variable?');

insert into answer(text, question_id, correct) values('17, byte', 27, false);
insert into answer(text, question_id, correct) values('25, byte', 27, false);
insert into answer(text, question_id, correct) values('17, int', 27, true);
insert into answer(text, question_id, correct) values('25, int', 27, false);

insert into question (text) values('What kind of thread is the Garbage collector thread is?');

insert into answer(text, question_id, correct) values('Non daemon thread', 28, false);
insert into answer(text, question_id, correct) values('Daemon thread', 28, true);
insert into answer(text, question_id, correct) values('Thread with dead state', 28, false);
insert into answer(text, question_id, correct) values('None of the above', 28, false);

insert into question (text) values('Which of the following statements is preferred to create a string "Welcome to Java Programming"?');

insert into answer(text, question_id, correct) values('String str = “Welcome to Java Programming”', 29, true);
insert into answer(text, question_id, correct) values('String str; str = “Welcome to Java Programming”', 29, false);
insert into answer(text, question_id, correct) values('String str = new String( “Welcome to Java Programming” )', 29, false);
insert into answer(text, question_id, correct) values('String str; str = new String (“Welcome to Java Programming” )', 29, false);

insert into question (text) values('Which of the following below is FALSE for abstract class?');

insert into answer(text, question_id, correct) values('An abstract class can have only abstract methods', 30, true);
insert into answer(text, question_id, correct) values('An abstract class can have both abstract and concrete methods', 30, false);
insert into answer(text, question_id, correct) values('An abstract class is to be extended by another class', 30, false);
insert into answer(text, question_id, correct) values('A class can extend only one abstract class', 30, false);

--Mykola

insert into question (text) values('What is the value of "d" after this line of code has been executed? <br><br> double d = Math.round ( 2.5 + Math.random() );');

insert into answer(text, question_id, correct) values('2', 31, false);
insert into answer(text, question_id, correct) values('3', 31, true);
insert into answer(text, question_id, correct) values('4', 31, false);
insert into answer(text, question_id, correct) values('2.5', 31, false);

insert into question (text) values('You want subclasses in any package to have access to members of a superclass. Which is the most restrictive access that accomplishes this objective?');

insert into answer(text, question_id, correct) values('public', 32, false);
insert into answer(text, question_id, correct) values('private', 32, false);
insert into answer(text, question_id, correct) values('protected', 32, true);
insert into answer(text, question_id, correct) values('transient', 32, false);

insert into question (text) values('Which three form part of correct array declarations? <br><br> 1. public int a [ ] <br> 2. static int [ ] a <br> 3. public [ ] int a <br> 4. private int a [3] <br> 5. private int [3] a [ ] <br> 6. public final int [ ] a');

insert into answer(text, question_id, correct) values('1', 33, true);
insert into answer(text, question_id, correct) values('2', 33, true);
insert into answer(text, question_id, correct) values('3', 33, false);
insert into answer(text, question_id, correct) values('4', 33, false);
insert into answer(text, question_id, correct) values('5', 33, false);
insert into answer(text, question_id, correct) values('6', 33, true);

insert into question (text) values('public class Test { } <br><br> What is the prototype of the default constructor?');

insert into answer(text, question_id, correct) values('Test( )', 34, false);
insert into answer(text, question_id, correct) values('Test(void)', 34, false);
insert into answer(text, question_id, correct) values('public Test( )', 34, true);
insert into answer(text, question_id, correct) values('public Test(void)', 34, false);

insert into question (text) values('What is the most restrictive access modifier that will allow members of one class to have access to members of another class in the same package?');

insert into answer(text, question_id, correct) values('public', 35, false);
insert into answer(text, question_id, correct) values('abstract', 35, false);
insert into answer(text, question_id, correct) values('protected', 35, false);
insert into answer(text, question_id, correct) values('synchronized', 35, false);
insert into answer(text, question_id, correct) values('default access', 35, true);

insert into question (text) values('Which of the following is/are legal method declarations? <br><br>1. protected abstract void m1();<br>2. static final void m1(){}<br>3. synchronized public final void m1() {}<br>4. private native void m1();');

insert into answer(text, question_id, correct) values('1', 36, true);
insert into answer(text, question_id, correct) values('2', 36, true);
insert into answer(text, question_id, correct) values('3', 36, true);
insert into answer(text, question_id, correct) values('4', 36, true);
insert into answer(text, question_id, correct) values('none', 36, false);

insert into question (text) values('Which three are valid method signatures in an interface?<br><br>1. private int getArea();<br>2. public float getVol(float x);<br>3. public void main(String [] args);<br>4. public static void main(String [] args);<br>5. boolean setFlag(Boolean [] test);');

insert into answer(text, question_id, correct) values('1', 37, false);
insert into answer(text, question_id, correct) values('2', 37, true);
insert into answer(text, question_id, correct) values('3', 37, true);
insert into answer(text, question_id, correct) values('4', 37, false);
insert into answer(text, question_id, correct) values('5', 37, true);

insert into question (text) values('Which of the following class level (nonlocal) variable declarations will not compile?');

insert into answer(text, question_id, correct) values('protected int a;', 38, false);
insert into answer(text, question_id, correct) values('transient int b = 3;', 38, false);
insert into answer(text, question_id, correct) values('private synchronized int e;', 38, true);
insert into answer(text, question_id, correct) values('volatile int d;', 38, false);

insert into question (text) values('Which four options describe the correct default values for array elements of the types indicated?');

insert into answer(text, question_id, correct) values('int -> 0', 39, true);
insert into answer(text, question_id, correct) values('String -> "null"', 39, false);
insert into answer(text, question_id, correct) values('Dog -> null', 39, true);
insert into answer(text, question_id, correct) values('char -> ''\u0000''', 39, true);
insert into answer(text, question_id, correct) values('float -> 0.0f', 39, true);
insert into answer(text, question_id, correct) values('boolean -> true', 39, false);

insert into question (text) values('Which is a reserved word in the Java programming language?');

insert into answer(text, question_id, correct) values('method', 40, false);
insert into answer(text, question_id, correct) values('native', 40, true);
insert into answer(text, question_id, correct) values('subclasses', 40, false);
insert into answer(text, question_id, correct) values('reference', 40, false);
insert into answer(text, question_id, correct) values('array', 40, false);

insert into question (text) values('Which is a valid keyword in java?');

insert into answer(text, question_id, correct) values('interface', 41, true);
insert into answer(text, question_id, correct) values('string', 41, false);
insert into answer(text, question_id, correct) values('Float', 41, false);
insert into answer(text, question_id, correct) values('unsigned', 41, false);

insert into question (text) values('Which three are valid declarations of a float?');

insert into answer(text, question_id, correct) values('float f1 = -343;', 42, true);
insert into answer(text, question_id, correct) values('float f2 = 3.14;', 42, false);
insert into answer(text, question_id, correct) values('float f3 = 0x12345;', 42, true);
insert into answer(text, question_id, correct) values('float f4 = 42e7;', 42, false);
insert into answer(text, question_id, correct) values('float f5 = 2001.0D;', 42, false);
insert into answer(text, question_id, correct) values('float f6 = 2.81F;', 42, true);

insert into question (text) values('Which is a valid declarations of a String?');

insert into answer(text, question_id, correct) values('String s1 = null;', 43, true);
insert into answer(text, question_id, correct) values('String s2 = ''null'';', 43, false);
insert into answer(text, question_id, correct) values('String s3 = (String) ''abc'';', 43, false);
insert into answer(text, question_id, correct) values('String s4 = (String) ''\ufeed'';', 43, false);

insert into question (text) values('What is the numerical range of a char?');

insert into answer(text, question_id, correct) values('-128 to 127', 44, false);
insert into answer(text, question_id, correct) values('-(2^15) to (2^15)-1', 44, false);
insert into answer(text, question_id, correct) values('0 to 32767', 44, false);
insert into answer(text, question_id, correct) values('0 to 65535', 44, true);

insert into question (text) values('Suppose that you would like to create an instance of a new Map that has an iteration order that is the same as the iteration order of an existing instance of a Map. Which concrete implementation of the Map interface should be used for the new instance?');

insert into answer(text, question_id, correct) values('TreeMap', 45, false);
insert into answer(text, question_id, correct) values('HashMap', 45, false);
insert into answer(text, question_id, correct) values('LinkedHashMap', 45, true);
insert into answer(text, question_id, correct) values('The answer depends on the implementation of the existing instance.', 45, false);

insert into question (text) values('Which class does not override the equals() and hashCode() methods, inheriting them directly from class Object?');

insert into answer(text, question_id, correct) values('java.lang.String', 46, false);
insert into answer(text, question_id, correct) values('java.lang.Double', 46, false);
insert into answer(text, question_id, correct) values('java.lang.StringBuffer', 46, true);
insert into answer(text, question_id, correct) values('java.lang.Character', 46, false);

insert into question (text) values('Which collection class allows you to grow or shrink its size and provides indexed access to its elements, but whose methods are not synchronized?');

insert into answer(text, question_id, correct) values('java.util.HashSet', 47, false);
insert into answer(text, question_id, correct) values('java.util.LinkedHashSet', 47, false);
insert into answer(text, question_id, correct) values('java.util.List', 47, false);
insert into answer(text, question_id, correct) values('java.util.ArrayList', 47, true);

insert into question (text) values('You need to store elements in a collection that guarantees that no duplicates are stored and all elements can be accessed in natural order. Which interface provides that capability?');

insert into answer(text, question_id, correct) values('java.util.Map', 48, false);
insert into answer(text, question_id, correct) values('java.util.Set', 48, true);
insert into answer(text, question_id, correct) values('java.util.List', 48, false);
insert into answer(text, question_id, correct) values('java.util.Collection', 48, false);

insert into question (text) values('Which interface does java.util.Hashtable implement?');

insert into answer(text, question_id, correct) values('Java.util.Map', 49, true);
insert into answer(text, question_id, correct) values('Java.util.List', 49, false);
insert into answer(text, question_id, correct) values('Java.util.HashTable', 49, false);
insert into answer(text, question_id, correct) values('Java.util.Collection', 49, false);

insert into question (text) values('Which interface provides the capability to store objects using a key-value pair?');

insert into answer(text, question_id, correct) values('Java.util.Map', 50, true);
insert into answer(text, question_id, correct) values('Java.util.Set', 50, false);
insert into answer(text, question_id, correct) values('Java.util.List', 50, false);
insert into answer(text, question_id, correct) values('Java.util.Collection', 50, false);

insert into question (text) values('Which collection class allows you to associate its elements with key values, and allows you to retrieve objects in FIFO (first-in, first-out) sequence?');

insert into answer(text, question_id, correct) values('java.util.ArrayList', 51, false);
insert into answer(text, question_id, correct) values('java.util.LinkedHashMap', 51, true);
insert into answer(text, question_id, correct) values('java.util.HashMap', 51, false);
insert into answer(text, question_id, correct) values('java.util.TreeMap', 51, false);

insert into question (text) values('Which collection class allows you to access its elements by associating a key with an element''s value, and provides synchronization?');

insert into answer(text, question_id, correct) values('java.util.SortedMap', 52, false);
insert into answer(text, question_id, correct) values('java.util.TreeMap', 52, false);
insert into answer(text, question_id, correct) values('java.util.TreeSet', 52, false);
insert into answer(text, question_id, correct) values('java.util.Hashtable', 52, true);

insert into question (text) values('Which is valid declaration of a float?');

insert into answer(text, question_id, correct) values('float f = 1F;', 53, true);
insert into answer(text, question_id, correct) values('float f = 1.0;', 53, false);
insert into answer(text, question_id, correct) values('float f = "1";', 53, false);
insert into answer(text, question_id, correct) values('float f = 1.0d;', 53, false);

insert into question (text) values('What is the numerical range of char?');

insert into answer(text, question_id, correct) values('	0 to 32767', 54, false);
insert into answer(text, question_id, correct) values('0 to 65535', 54, true);
insert into answer(text, question_id, correct) values('-256 to 255', 54, false);
insert into answer(text, question_id, correct) values('-32768 to 32767', 54, false);

insert into question (text) values('Which of the following are Java reserved words?');

insert into answer(text, question_id, correct) values('run', 55, false);
insert into answer(text, question_id, correct) values('import', 55, true);
insert into answer(text, question_id, correct) values('default', 55, true);
insert into answer(text, question_id, correct) values('implement', 55, false);

insert into question (text) values('Which of the collection classes will follow the InsertionOrder');

insert into answer(text, question_id, correct) values('ArrayList', 56, false);
insert into answer(text, question_id, correct) values('LinkedHash set', 56, false);
insert into answer(text, question_id, correct) values('Vector', 56, false);
insert into answer(text, question_id, correct) values('All the above', 56, true);

insert into question (text) values('Which inheritance is not supported in java?');

insert into answer(text, question_id, correct) values('Single inheritance', 57, false);
insert into answer(text, question_id, correct) values('Hybrid inheritance', 57, false);
insert into answer(text, question_id, correct) values('Multilevel inheritance', 57, false);
insert into answer(text, question_id, correct) values('Java supports all of the above ', 57, true);

insert into question (text) values('Which is a mechanism in which one object acquires all the properties and behaviors of parent object?');

insert into answer(text, question_id, correct) values('Inheritance', 58, true);
insert into answer(text, question_id, correct) values('Encapsulation', 58, false);
insert into answer(text, question_id, correct) values('Polymorphism', 58, false);
insert into answer(text, question_id, correct) values('None of the above', 58, false);

insert into question (text) values('If subclass (child class) has the same method as declared in the parent class, it is known as?');

insert into answer(text, question_id, correct) values('Method overriding', 59, true);
insert into answer(text, question_id, correct) values('Method overloading', 59, false);
insert into answer(text, question_id, correct) values('Constructor overloading', 59, false);
insert into answer(text, question_id, correct) values('None of the above', 59, false);

insert into question (text) values('In Method overriding a subclass in a different package can only override the non-final methods declared public or protected?');

insert into answer(text, question_id, correct) values('True', 60, true);
insert into answer(text, question_id, correct) values('False', 60, false);

