package ua.eurosoftware.quizbon.repository;

import java.util.List;

import ua.eurosoftware.quizbon.dto.UserDetailedStatistic;
import ua.eurosoftware.quizbon.model.User;

public interface UserRepository {
	
	Long save(User user);
	
	Boolean getUserByEmail(String email);
	
	Boolean getUserByPhone(String phone);
	
	List<UserDetailedStatistic> getUsersDetailedStatisticByOffset(int count, int offset);
	
	Long countUsers();

}
