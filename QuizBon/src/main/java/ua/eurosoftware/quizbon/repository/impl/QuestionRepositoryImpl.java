package ua.eurosoftware.quizbon.repository.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ua.eurosoftware.quizbon.model.Question;
import ua.eurosoftware.quizbon.repository.QuestionRepository;
import ua.eurosoftware.quizbon.repository.config.BaseJDBCTemplate;
import ua.eurosoftware.quizbon.util.PropertiesUtil;
import ua.eurosoftware.quizbon.util.PropertiesUtil.PropertyType;

@Repository
public class QuestionRepositoryImpl extends BaseJDBCTemplate implements QuestionRepository {

	private static final Logger logger = Logger.getLogger(QuestionRepositoryImpl.class);
	private static final String QUESTION_TABLE = "question";

	@Transactional
	@Override
	public long add(Question question) {
		logger.debug("saving question " + question);
		String sql = PropertiesUtil.getProperty(PropertyType.SQL, QUESTION_TABLE, "saveQuestion");
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		jdbcTemplate.update(connection -> {
			PreparedStatement ps = connection.prepareStatement(sql, new String[] {"question_id"});
			ps.setString(1, question.getText());
			return ps;
		}, keyHolder);

		return keyHolder.getKey().longValue();
	}

	@Transactional
	@Override
	public void remove(long id) {
		logger.debug("deleting question with id " + id);
		String sql = PropertiesUtil.getProperty(PropertyType.SQL, QUESTION_TABLE, "deleteQuestion");
		jdbcTemplate.update(sql, id);
	}

	@Transactional
	@Override
	public void update(Question question) {
		logger.debug("updating question " + question);
		String sql = PropertiesUtil.getProperty(PropertyType.SQL, QUESTION_TABLE, "updateQuestion");
		jdbcTemplate.update(sql, question.getText(), question.getId());
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<Question> getAll() {
		logger.debug("getting all questions");
		String sql = PropertiesUtil.getProperty(PropertyType.SQL, QUESTION_TABLE, "getAll");
		List<Question> rows = jdbcTemplate.query(sql, questionMapper);
		return rows;
	}

	@Transactional(readOnly = true)
	@Override
	public List<Question> getPartByOffset(int offset, int count) {
		String sql = PropertiesUtil.getProperty(PropertyType.SQL, QUESTION_TABLE, "getAllByOffset");
		List<Question> rows = jdbcTemplate.query(sql, questionMapper, count, offset);
		return rows;
	}

	@Transactional(readOnly = true)
	@Override
	public Question getById(int id) {
		logger.debug("getting question with " + id);
		String sql = PropertiesUtil.getProperty(PropertyType.SQL, QUESTION_TABLE, "getById");
		Question question = jdbcTemplate.queryForObject(sql, questionMapper, id);
		return question;
	}

	@Transactional(readOnly = true)
	@Override
	public int getQuestionCount() {
		String sql = PropertiesUtil.getProperty(PropertyType.SQL, QUESTION_TABLE, "getQuestionCount");
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}
	
	@Transactional
	@Override
	public void switchQuestionState(Question question) {
		String sql = PropertiesUtil.getProperty(PropertyType.SQL, QUESTION_TABLE, "switchQuestionState");
		jdbcTemplate.update(sql, question.isEnabled(), question.getId());
	}

	RowMapper<Question> questionMapper = (ResultSet result, int row) -> {
		Question question = new Question();
		question.setId(result.getLong("question_id"));
		question.setText(result.getString("text"));
		question.setEnabled(result.getBoolean("active"));
		return question;
	};

	@Transactional(readOnly = true)
	@Override
	public List<Question> getRandomQuestions(int limit) {
		logger.debug("getting random question list");
		
		String sql = PropertiesUtil.getProperty(PropertyType.SQL, QUESTION_TABLE, "getRandomQuestions");
		List<Question> randomQuestions = new ArrayList<>();
		
		try {
			randomQuestions = jdbcTemplate.query(sql, new Object[]{limit}, questionMapper);
		} catch (DataAccessException e) {
			logger.error(e.getCause(), e);
		}
		
		return randomQuestions;
	}

}
