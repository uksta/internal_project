package ua.eurosoftware.quizbon.repository.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import ua.eurosoftware.quizbon.model.Answer;
import ua.eurosoftware.quizbon.repository.AnswerRepository;
import ua.eurosoftware.quizbon.repository.config.BaseJDBCTemplate;
import ua.eurosoftware.quizbon.util.PropertiesUtil;
import ua.eurosoftware.quizbon.util.PropertiesUtil.PropertyType;

@Repository
public class AnswerRepositoryImpl extends BaseJDBCTemplate implements AnswerRepository {

	private static final Logger logger = Logger.getLogger(AnswerRepositoryImpl.class);
	private static final String ANSWER_TABLE = "answer";

	@Override
	public List<Answer> getAnswersByQuestionId(Long id) {
		logger.debug("getting answer list for question with id=" + id);

		final String sql = PropertiesUtil.getProperty(PropertyType.SQL, ANSWER_TABLE, "getAnswersByQuestionId");
		List<Answer> answers = new ArrayList<>();

		try {
			answers = jdbcTemplate.query(sql, new Object[] { id }, new AnswerMapper());
		} catch (DataAccessException e) {
			logger.error(e.getCause(), e);
		}

		return answers;
	}

	private class AnswerMapper implements RowMapper<Answer> {

		@Override
		public Answer mapRow(ResultSet rs, int rowNum) throws SQLException {
			Answer answer = new Answer();
			answer.setId(rs.getLong("answer_id"));
			answer.setText(rs.getString("text"));
			answer.setQuestionId(rs.getLong("question_id"));
			answer.setCorrect(rs.getBoolean("correct"));
			answer.setActive(rs.getBoolean("active"));
			return answer;
		}

	}

	
	@Override
	public void saveAnswersList(final List<Answer> answers) {
		removeEmptyAnswers(answers);
		String sql = PropertiesUtil.getProperty(PropertyType.SQL, ANSWER_TABLE, "saveAnswer");

		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Answer answer = answers.get(i);
				ps.setString(1, answer.getText());
				ps.setLong(2, answer.getQuestionId());
				ps.setBoolean(3, answer.isCorrect());
			}
			@Override
			public int getBatchSize() {
				return answers.size();
			}
		});
	}

	@Override
	public void updateAnswers(List<Answer> answers) {
		final String sql = PropertiesUtil.getProperty(PropertyType.SQL, ANSWER_TABLE, "updateAnswers");
		answers.forEach((answer) -> {
			jdbcTemplate.update(sql, answer.isActive(), answer.getId());
		});
	}
	
	private void removeEmptyAnswers(List<Answer> answers) {
		answers.removeIf(answer -> answer.getText().equals(""));
	}
}
