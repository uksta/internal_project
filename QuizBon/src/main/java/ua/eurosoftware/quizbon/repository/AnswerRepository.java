package ua.eurosoftware.quizbon.repository;

import java.util.List;

import ua.eurosoftware.quizbon.model.Answer;

public interface AnswerRepository {

	public List<Answer> getAnswersByQuestionId(Long id);
	public void saveAnswersList(List<Answer> answers);
	public void updateAnswers(List<Answer> answers);
	
}
