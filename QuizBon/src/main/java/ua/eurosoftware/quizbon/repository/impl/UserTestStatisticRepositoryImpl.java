package ua.eurosoftware.quizbon.repository.impl;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import ua.eurosoftware.quizbon.model.UserTestStatistic;
import ua.eurosoftware.quizbon.repository.UserTestStatisticRepository;
import ua.eurosoftware.quizbon.repository.config.BaseJDBCTemplate;
import ua.eurosoftware.quizbon.util.PropertiesUtil;
import ua.eurosoftware.quizbon.util.PropertiesUtil.PropertyType;

@Repository
public class UserTestStatisticRepositoryImpl extends BaseJDBCTemplate implements UserTestStatisticRepository {

	private static final Logger logger = Logger.getLogger(UserTestStatisticRepositoryImpl.class);
	private static final String USER_TEST_STATISTIC_TABLE = "user_test_statistic";

	@Override
	public void save(UserTestStatistic userTestStatistic) {
		logger.debug("create new user test statistic");

		final String sql = PropertiesUtil.getProperty(PropertyType.SQL, USER_TEST_STATISTIC_TABLE,
				"saveUserTestStatistic");

		try {
			jdbcTemplate.update(sql,
					new Object[] { userTestStatistic.getUserId(), userTestStatistic.getResult(),
							userTestStatistic.getTestMax(), userTestStatistic.getTestStartTime(),
							userTestStatistic.getTestEndTime() });
		} catch (DataAccessException e) {
			logger.error(e.getCause(), e);
		}

	}

}
