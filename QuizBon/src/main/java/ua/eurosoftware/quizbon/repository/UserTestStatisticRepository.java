package ua.eurosoftware.quizbon.repository;

import ua.eurosoftware.quizbon.model.UserTestStatistic;

public interface UserTestStatisticRepository {
	
	void save(UserTestStatistic userTestStatistic);
	
}
