package ua.eurosoftware.quizbon.repository;

import java.util.List;

import ua.eurosoftware.quizbon.model.Question;


public interface QuestionRepository {
	
	public long add(Question question);
	public void remove(long id);
	public void update(Question question);
	public List<Question> getAll();
	public List<Question> getPartByOffset(int offset, int count);
	public List<Question> getRandomQuestions(int limit);
	public Question getById(int id);
	public int getQuestionCount();
	public void switchQuestionState(Question question);
}
