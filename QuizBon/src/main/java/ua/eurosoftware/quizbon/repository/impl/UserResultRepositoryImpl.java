package ua.eurosoftware.quizbon.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ua.eurosoftware.quizbon.dto.Result;
import ua.eurosoftware.quizbon.model.Answer;
import ua.eurosoftware.quizbon.model.Question;
import ua.eurosoftware.quizbon.repository.UserResultRepository;
import ua.eurosoftware.quizbon.repository.config.BaseJDBCTemplate;
import ua.eurosoftware.quizbon.util.PropertiesUtil;
import ua.eurosoftware.quizbon.util.PropertiesUtil.PropertyType;

@Repository
public class UserResultRepositoryImpl extends BaseJDBCTemplate implements UserResultRepository{

	private static final  Logger logger = Logger.getLogger(UserResultRepositoryImpl.class);
	
	private static final String USER_RESULT_TABLE = "user_result";
	
	@Override
	public Long saveUserQuestion(Long userId, Long questionId) {
		logger.debug("create() user_question for user with id: " + userId);
		
		final String sql = PropertiesUtil.getProperty(PropertyType.SQL, USER_RESULT_TABLE, "saveUserQuestion");
		
		try {
			KeyHolder keyHolder = new GeneratedKeyHolder();
			jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
						PreparedStatement ps = connection.prepareStatement(sql, new String[] {"user_question_id"});
						ps.setLong(1, userId);
						ps.setLong(2, questionId);
						return ps;
			}
			}, keyHolder);
			logger.debug("New user_question with user_id: [" + userId + "] has been created");
			
			return keyHolder.getKey().longValue();
		} catch (DataAccessException e) {
			logger.error(e.getCause(), e);
		}
		
		return null;
	}

	@Override
	public void saveUserAnswer(Long userQuestionId, Long answerId, Boolean checked) {
		logger.debug("create() user_answer for user_question_id: " + userQuestionId);
		
		final String sql = PropertiesUtil.getProperty(PropertyType.SQL, USER_RESULT_TABLE, "saveUserAnswer");
		
		try {
			jdbcTemplate.update(sql, userQuestionId, answerId, checked);
			logger.info("New user_answers with question_id=[" + userQuestionId + "has been created");
			
		} catch (DataAccessException e) {
			logger.error(e.getCause(), e);
		}
		
	}

	@Transactional
	@Override
	public Result findByUserId(Long userId) {
		logger.debug("getting user result with user id=[" + userId + "]");
		
		final String sql = PropertiesUtil.getProperty(PropertyType.SQL, USER_RESULT_TABLE, "findByUserId");
		Result result = new Result();
		
		try {
			result = (Result) jdbcTemplate.queryForObject(sql, resultMapper, new Object[] { userId });
		} catch (EmptyResultDataAccessException e) {
			logger.info("No result found by " + userId);
		} catch (DataAccessException e) {
			logger.error(e.getCause(), e);
		}

		return result;
	}
	
	RowMapper<Result> resultMapper = (ResultSet resultSet, int row) -> {
		
		Question question = new Question();
		Result result = new Result();
		List<Question> questions = new ArrayList<>();
		List<Answer> answers = new ArrayList<>();
		
		do {
			if (question.getId() != (Long) resultSet.getLong("question_id")) {
				if (question.getId() != null) {
					question.setAnswers(answers);
					questions.add(question);
				}
				question = new Question();
				question.setId(resultSet.getLong("question_id"));
				question.setText(resultSet.getString("question_text"));
				answers = new ArrayList<>();
				answers.add(new Answer(resultSet.getLong("answer_id"), resultSet.getString("answer_text"), 
						resultSet.getLong("question_id"), resultSet.getBoolean("correct"), resultSet.getBoolean("checked")));
			} else {
				answers.add(new Answer(resultSet.getLong("answer_id"), resultSet.getString("answer_text"),
						resultSet.getLong("question_id"), resultSet.getBoolean("correct"), resultSet.getBoolean("checked")));
				if(resultSet.isLast()) {
					question.setAnswers(answers);
					questions.add(question);
				}
			}
		} while (resultSet.next());
		
		result.setQuestions(questions);
		
		return result;
	};
	
}
