package ua.eurosoftware.quizbon.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ua.eurosoftware.quizbon.dto.UserDetailedStatistic;
import ua.eurosoftware.quizbon.model.User;
import ua.eurosoftware.quizbon.model.UserTestStatistic;
import ua.eurosoftware.quizbon.repository.UserRepository;
import ua.eurosoftware.quizbon.repository.config.BaseJDBCTemplate;
import ua.eurosoftware.quizbon.util.PropertiesUtil;
import ua.eurosoftware.quizbon.util.PropertiesUtil.PropertyType;

@Repository
public class UserRepositoryImpl extends BaseJDBCTemplate implements UserRepository {

	private static final Logger logger = Logger.getLogger(UserRepositoryImpl.class);
	private static final String USERS_TABLE = "users";

	public Long save(final User user) {
		logger.debug("create new user with email: " + user.getEmail());
		
		final String sql = PropertiesUtil.getProperty(PropertyType.SQL, USERS_TABLE, "saveUser");

		try {
			KeyHolder keyHolder = new GeneratedKeyHolder();
			jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
						PreparedStatement ps = connection.prepareStatement(sql, new String[] {"user_id"});
						ps.setString(1, user.getFirstName());
						ps.setString(2, user.getLastName());
						ps.setString(3, user.getPhone());
						ps.setString(4, user.getEmail());
						ps.setString(5, user.getSkype());
						return ps;
			}
			}, keyHolder);
			logger.debug("New user[ " + user.getEmail() + "] has been created");
			
			return keyHolder.getKey().longValue();
		} catch (DataAccessException e) {
			logger.error(e.getCause(), e);
		}
		
		return null;
	}

	@Transactional
	@Override
	public Boolean getUserByEmail(final String email) {
		logger.debug("looking for user with email: " + email);
		
		final String sql = PropertiesUtil.getProperty(PropertyType.SQL, USERS_TABLE, "findUserByEmail");
		
		return findUser(sql, email);
	}

	@Transactional
	@Override
	public Boolean getUserByPhone(String phone) {
		logger.debug("looking for user with phone: " + phone);
		
		final String sql = PropertiesUtil.getProperty(PropertyType.SQL, USERS_TABLE, "findUserByPhone");
		
		return findUser(sql, phone);
	}

	private Boolean findUser(String sql, String paramValue) {
		
		try {
			User user = (User)jdbcTemplate.queryForObject(sql, new UserSearchRowMapper(), new Object[] { paramValue });
			return (user != null) ? true : false;
		} catch (EmptyResultDataAccessException e) {
			logger.info("No result found by " + paramValue);
		} catch (DataAccessException e) {
			logger.error(e.getCause(), e);
		}
		
		return false;
	}

	@Transactional
	public List<UserDetailedStatistic> getUsersDetailedStatisticByOffset(int count, int offset) {
		logger.debug("gettin list of users");
		
		final String sql = PropertiesUtil.getProperty(PropertyType.SQL, USERS_TABLE, "getUserDetailedStatisticByOffset");
		try {
			return jdbcTemplate.query(sql, new UserDetailedStatisticRowMapper(), new Object[]{count, offset});
		} catch (DataAccessException e) {
			logger.error(e.getCause(), e);
		}
		return new ArrayList<>();
	}

	@Transactional
	@Override
	public Long countUsers() {
		logger.debug("count all users");
		
		final String sql = PropertiesUtil.getProperty(PropertyType.SQL, USERS_TABLE, "countUsers");
		try {
			return jdbcTemplate.queryForObject(sql, Long.class);
		} catch (DataAccessException e) {
			logger.error(e.getCause(), e);
		}
		return new Long(0);
	}
	
	private class UserSearchRowMapper implements RowMapper<User> {

		@Override
		public User mapRow(ResultSet rs, int arg1) throws SQLException {
			User user = new User();
			user.setEmail(rs.getString("email"));
			user.setPhone(rs.getString("phone"));
			return user;
		}
		
	}
	
	private class UserDetailedStatisticRowMapper implements RowMapper<UserDetailedStatistic> {

		@Override
		public UserDetailedStatistic mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			user.setId(rs.getLong("user_id"));
			user.setFirstName(rs.getString("first_name"));
			user.setLastName(rs.getString("last_name"));
			user.setEmail(rs.getString("email"));
			user.setPhone(rs.getString("phone"));
			user.setSkype(rs.getString("skype"));
			user.setActive(rs.getBoolean("active"));
			user.setCreationDate(rs.getTimestamp("creation_date"));
			
			UserTestStatistic userTestStatistic = new UserTestStatistic();
			userTestStatistic.setResult(rs.getInt("result"));
			userTestStatistic.setTestMax(rs.getInt("test_max"));
			userTestStatistic.setTestStartTime(rs.getTimestamp("test_start_time"));
			userTestStatistic.setTestEndTime(rs.getTimestamp("test_end_time"));
			
			return new UserDetailedStatistic(user, userTestStatistic);
		}

	}

}
