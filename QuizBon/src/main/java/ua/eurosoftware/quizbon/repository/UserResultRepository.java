package ua.eurosoftware.quizbon.repository;

import ua.eurosoftware.quizbon.dto.Result;

public interface UserResultRepository {

	Result findByUserId(Long userId);
	
	Long saveUserQuestion(Long userId, Long questionId);
	
	void saveUserAnswer(Long userQuestionId, Long answerId, Boolean checked);
	
}
