package ua.eurosoftware.quizbon.dto;

import java.util.Collection;
import java.util.List;

import ua.eurosoftware.quizbon.model.Question;

public class Quiz {

	private Integer secondsPerQuestion;
	private Collection<Question> testQuestions;

	public Quiz(Integer secondsPerQuestion, Collection<Question> testQuestions) {
		this.secondsPerQuestion = secondsPerQuestion;
		this.testQuestions = testQuestions;
	}

	public Integer getSecondsPerQuestion() {
		return secondsPerQuestion;
	}

	public void setSecondsPerQuestion(Integer secondsPerQuestion) {
		this.secondsPerQuestion = secondsPerQuestion;
	}

	public Collection<Question> getTestQuestions() {
		return testQuestions;
	}

	public void setTestQuestions(List<Question> testQuestion) {
		this.testQuestions = testQuestion;
	}

}
