package ua.eurosoftware.quizbon.dto;

import ua.eurosoftware.quizbon.model.User;
import ua.eurosoftware.quizbon.model.UserTestStatistic;

public class UserDetailedStatistic {

	private User user;
	private UserTestStatistic userTestStatistic;
	
	public UserDetailedStatistic(User user, UserTestStatistic userTestStatistic) {
		this.user = user;
		this.userTestStatistic = userTestStatistic;
	}

	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public UserTestStatistic getUserTestStatistic() {
		return userTestStatistic;
	}
	
	public void setUserTestStatistic(UserTestStatistic userTestStatistic) {
		this.userTestStatistic = userTestStatistic;
	}
	
}
