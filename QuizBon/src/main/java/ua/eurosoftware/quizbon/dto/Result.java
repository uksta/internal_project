package ua.eurosoftware.quizbon.dto;

import java.util.ArrayList;
import java.util.List;

import ua.eurosoftware.quizbon.model.Question;
import ua.eurosoftware.quizbon.model.User;
import ua.eurosoftware.quizbon.model.UserTestStatistic;

public class Result {

	private User user;
	private UserTestStatistic userTestStatistic;
	private List<Question> questions = new ArrayList<>();

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public UserTestStatistic getUserTestStatistic() {
		return userTestStatistic;
	}

	public void setUserTestStatistic(UserTestStatistic userTestStatistic) {
		this.userTestStatistic = userTestStatistic;
	}

}
