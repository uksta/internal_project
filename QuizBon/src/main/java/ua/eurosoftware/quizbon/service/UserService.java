package ua.eurosoftware.quizbon.service;

import java.util.List;

import ua.eurosoftware.quizbon.dto.UserDetailedStatistic;
import ua.eurosoftware.quizbon.model.User;

public interface UserService {
	
	void saveUser(User user);
	
	boolean isEmailExist(String email);
	
	boolean isPhoneExist(String phone);
	
	List<UserDetailedStatistic> getAllUserStatistics(int count, int offset);
	
	long getAllUsersCount();
	
}
