package ua.eurosoftware.quizbon.service;

import java.util.List;

import ua.eurosoftware.quizbon.dto.Quiz;
import ua.eurosoftware.quizbon.model.Answer;
import ua.eurosoftware.quizbon.model.Question;

public interface QuizService {

	public int getQuestionCount();
		
	public void add(Question question);
	
	public void remove(long id);
	
	public void update(Question question);
	
	public List<Question> getQuestions();
	
	public List<Question> getPartByOffset(int offset, int count);
	
	public Question getById(int id);
	
	Quiz generateQuiz();
	
	public List<Answer> getAnswersByQuestionId(long questionId);

	public void switchQuestionState(Question question);
}
