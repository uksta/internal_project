package ua.eurosoftware.quizbon.service.impl;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ua.eurosoftware.quizbon.dto.Result;
import ua.eurosoftware.quizbon.model.Answer;
import ua.eurosoftware.quizbon.model.Question;
import ua.eurosoftware.quizbon.repository.UserRepository;
import ua.eurosoftware.quizbon.repository.UserResultRepository;
import ua.eurosoftware.quizbon.repository.UserTestStatisticRepository;
import ua.eurosoftware.quizbon.service.ResultService;

@Service
public class ResultServiceImpl implements ResultService {
	
	@Inject
	private UserRepository userRepository;
	
	@Inject
	private UserResultRepository userResultRepository;
	
	@Inject
	private UserTestStatisticRepository userTestStatisticRepository;
	
	@Override
	public Result getResultByUserId(long userId) {
		return userResultRepository.findByUserId(userId);
	}

	@Transactional
	@Override
	public void saveResult(Result result) {
		long userId = userRepository.save(result.getUser());
		result.getUserTestStatistic().setUserId(userId);
		userTestStatisticRepository.save(result.getUserTestStatistic());
		
		result.getQuestions().forEach(question->{
			
			Long userQuizId = userResultRepository.saveUserQuestion(userId, question.getId());
			
			question.getAnswers().forEach(answer->userResultRepository.saveUserAnswer(userQuizId, answer.getId(), answer.isChecked()));
			
		});
	}

	@Override
	public int calculateUserRightAnswers(Result result) {
		int rightAnswersCount = result.getQuestions().size();
		
		for (Question question : result.getQuestions()) {
			boolean isCorrect = true;
			for(Answer answer : question.getAnswers()) {
				if((answer.isChecked() && !answer.isCorrect()) || (!answer.isChecked() && answer.isCorrect())) {
					isCorrect = false;
				}
			}
			if(!isCorrect) rightAnswersCount--;
		}

		return rightAnswersCount;
	}

}
