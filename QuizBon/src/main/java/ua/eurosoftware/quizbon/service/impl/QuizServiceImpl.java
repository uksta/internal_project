package ua.eurosoftware.quizbon.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ua.eurosoftware.quizbon.dto.Quiz;
import ua.eurosoftware.quizbon.model.Answer;
import ua.eurosoftware.quizbon.model.Question;
import ua.eurosoftware.quizbon.model.QuizSettings;
import ua.eurosoftware.quizbon.repository.AnswerRepository;
import ua.eurosoftware.quizbon.repository.QuestionRepository;
import ua.eurosoftware.quizbon.service.QuizService;
import ua.eurosoftware.quizbon.service.SettingsService;

@Service
public class QuizServiceImpl implements QuizService {

	@Inject
	private QuestionRepository questionRepository;
	@Inject
	private AnswerRepository answerRepository;
	@Inject
	private SettingsService settingsService;

	@Transactional(readOnly = true)
	@Override
	public Quiz generateQuiz() {
		QuizSettings settings = settingsService.getSettings();
		Quiz quiz = new Quiz(settings.getTestTime(), getRandomQuestions(settings.getQuestionsCount()));
		return quiz;
	}

	private List<Question> getRandomQuestions(int questionsCount) {
		List<Question> randomQuestions = questionRepository.getRandomQuestions(questionsCount);
		
		randomQuestions.forEach(question -> {
			List<Answer> answers = answerRepository.getAnswersByQuestionId(question.getId());
			Collections.shuffle(answers);
			question.setAnswers(answers);
		});

		return randomQuestions;
	}

	@Transactional
	@Override
	public void add(Question question) {
		if (!question.getText().equals("")) {
			long questionId = questionRepository.add(question);
			tieAnswersToQuestionId(questionId, question.getAnswers());
			answerRepository.saveAnswersList(question.getAnswers());
		}
	}

	private void tieAnswersToQuestionId(long questionId, List<Answer> answers) {
		answers.forEach((answer) -> {
			answer.setQuestionId(questionId);
		});
	}

	@Transactional
	@Override
	public void remove(long id) {
		questionRepository.remove(id);
	}

	@Transactional
	@Override
	public void update(Question question) {
		questionRepository.update(question);
		List<Answer> answers = question.getAnswers();
		List<Answer> newAnswers = new ArrayList<>();
		
		Iterator<Answer> iterator = answers.iterator();
		while(iterator.hasNext()) {
			Answer answer = iterator.next();
			if(answer.getId() == null && !"".equals(answer.getText())) {
				answer.setQuestionId(question.getId());
				newAnswers.add(answer);
				iterator.remove();
			}
		}
		answerRepository.updateAnswers(answers);
		answerRepository.saveAnswersList(newAnswers);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Question> getPartByOffset(int offset, int count) {
		return questionRepository.getPartByOffset(offset, count);
	}

	@Transactional(readOnly = true)
	@Override
	public Question getById(int id) {
		return questionRepository.getById(id);
	}

	@Transactional(readOnly = true)
	@Override
	public int getQuestionCount() {
		return questionRepository.getQuestionCount();
	}

	@Transactional(readOnly = true)
	@Override
	public List<Question> getQuestions() {
		return questionRepository.getAll();
	}

	@Transactional(readOnly = true)
	@Override
	public List<Answer> getAnswersByQuestionId(long questionId) {
		return answerRepository.getAnswersByQuestionId(questionId);
	}
	@Transactional
	@Override
	public void switchQuestionState(Question question) {
		questionRepository.switchQuestionState(question);
	}

}
