package ua.eurosoftware.quizbon.service;

import ua.eurosoftware.quizbon.dto.Result;

public interface ResultService {

	
	Result getResultByUserId(long userId);
	
	void saveResult(Result result);
	
	int calculateUserRightAnswers(Result result);
}
