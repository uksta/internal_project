package ua.eurosoftware.quizbon.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import ua.eurosoftware.quizbon.dto.UserDetailedStatistic;
import ua.eurosoftware.quizbon.model.User;
import ua.eurosoftware.quizbon.repository.UserRepository;
import ua.eurosoftware.quizbon.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Inject
	private UserRepository userRepository;
	
	@Override
	public void saveUser(User user) {
		userRepository.save(user);
	}

	@Override
	public boolean isEmailExist(String email) {
		return userRepository.getUserByEmail(email);
	}

	@Override
	public boolean isPhoneExist(String phone) {
		return userRepository.getUserByPhone(phone);
	}

	@Override
	public List<UserDetailedStatistic> getAllUserStatistics(int count, int offset) {
		return userRepository.getUsersDetailedStatisticByOffset(count, offset);
	}

	@Override
	public long getAllUsersCount() {
		return userRepository.countUsers();
	}
}
