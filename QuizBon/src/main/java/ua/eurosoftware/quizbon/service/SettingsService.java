package ua.eurosoftware.quizbon.service;

import ua.eurosoftware.quizbon.model.QuizSettings;

public interface SettingsService {
	
	String CONFIGURATION_FILE_NAME = "test_config";
	String EXTENSION = ".properties";
	String QUESTION_COUNT_KEY = "questionsCount";
	String TEST_TIME_KEY = "testTime";

	void updatePropertyFile(QuizSettings quizSettings);

	QuizSettings getSettings();

}
