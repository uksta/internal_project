package ua.eurosoftware.quizbon.service.impl;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import ua.eurosoftware.quizbon.model.QuizSettings;
import ua.eurosoftware.quizbon.repository.impl.QuestionRepositoryImpl;
import ua.eurosoftware.quizbon.service.SettingsService;
import ua.eurosoftware.quizbon.util.PropertiesUtil;
import ua.eurosoftware.quizbon.util.PropertiesUtil.PropertyType;

@Service
public class SettingsServiceImpl implements SettingsService {
	private static final Logger logger = Logger.getLogger(QuestionRepositoryImpl.class);
	
	@Override
	public void updatePropertyFile(QuizSettings quizSettings) {

		PropertiesConfiguration config;
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			config = new PropertiesConfiguration(classLoader
					.getResource(PropertyType.SETTINGS.get() + CONFIGURATION_FILE_NAME + EXTENSION).getFile());
			config.setProperty(QUESTION_COUNT_KEY, quizSettings.getQuestionsCount());
			config.setProperty(TEST_TIME_KEY, quizSettings.getTestTime());
			config.save();
			logger.info("Config Property Successfully Updated...");
		} catch (ConfigurationException e) {
			logger.error("CANNOT READ OR SAVE PROPERTY FILE!!! : " + e);
		}
	}

	public QuizSettings getSettings() {
		QuizSettings settings = QuizSettings.getInstance();
		settings.setTestTime(
				PropertiesUtil.getPropertyIntValue(PropertyType.SETTINGS, CONFIGURATION_FILE_NAME, TEST_TIME_KEY));
		settings.setQuestionsCount(
				PropertiesUtil.getPropertyIntValue(PropertyType.SETTINGS, CONFIGURATION_FILE_NAME, QUESTION_COUNT_KEY));
		return settings;
	}

}