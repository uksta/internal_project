package ua.eurosoftware.quizbon.model;

import java.beans.Transient;
import java.io.Serializable;

import org.apache.commons.lang.builder.HashCodeBuilder;

public abstract class AbstractModel {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1561671420531224878L;

	@Transient
	public abstract Serializable getEntityId();

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(getEntityId()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AbstractModel)) {
			return false;
		}
		AbstractModel other = (AbstractModel) obj;
		if (getEntityId() == null) {
			if (other.getEntityId() != null) {
				return false;
			}
		} else if (!getEntityId().equals(other.getEntityId())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [id: " + this.getEntityId() + "]";
	}

}
