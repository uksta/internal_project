package ua.eurosoftware.quizbon.model;

public class QuizSettings {

	private int testTime;
	private int questionsCount;

	private static QuizSettings instance;

	private QuizSettings() {
	}

	public int getTestTime() {
		return testTime;
	}

	public void setTestTime(int testTime) {
		this.testTime = testTime;
	}

	public int getQuestionsCount() {
		return questionsCount;
	}

	public void setQuestionsCount(int questionsCount) {
		this.questionsCount = questionsCount;
	}
	
	public static QuizSettings getInstance() {
		if (instance == null) {
			instance = new QuizSettings();
		}
		return instance;
	}
}
