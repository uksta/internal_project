package ua.eurosoftware.quizbon.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserTestStatistic extends AbstractModel{

	private Long userTestStaticticId;
	private Long userId;
	private Integer result;
	private Integer testMax;
	private Timestamp testStartTime;
	private Timestamp testEndTime;
	
	public Long getUserTestStaticticId() {
		return userTestStaticticId;
	}

	public void setUserTestStaticticId(Long userTestStaticticId) {
		this.userTestStaticticId = userTestStaticticId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public Integer getTestMax() {
		return testMax;
	}

	public void setTestMax(Integer testMax) {
		this.testMax = testMax;
	}

	public Timestamp getTestStartTime() {
		return testStartTime;
	}

	public void setTestStartTime(Timestamp testStartTime) {
		this.testStartTime = testStartTime;
	}

	public Timestamp getTestEndTime() {
		return testEndTime;
	}

	public void setTestEndTime(Timestamp testEndTime) {
		this.testEndTime = testEndTime;
	}	
	
	public void setQuizTime(LocalDateTime quizStart, LocalDateTime quizEnd){
		this.testStartTime = Timestamp.valueOf(quizStart);
		this.testEndTime = Timestamp.valueOf(quizEnd);
	}
	
	@JsonIgnore
	@Override
	public Serializable getEntityId() {
		return getUserTestStaticticId();
	}

}
