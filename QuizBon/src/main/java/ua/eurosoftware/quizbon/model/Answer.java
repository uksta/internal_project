package ua.eurosoftware.quizbon.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Answer extends AbstractModel{

	private Long id;
	private String text;
	private Long questionId;
	private boolean correct;
	private boolean checked;
	private boolean active;
	
	public Answer(){}
	
	public Answer(Long id, String text, Long questionId, boolean right,  boolean checked) {
		this.id = id;
		this.text = text;
		this.questionId = questionId;
		this.correct = right;
		this.checked = checked;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean isRight) {
		this.correct = isRight;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean isChecked) {
		this.checked = isChecked;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@JsonIgnore
	@Override
	public Serializable getEntityId() {
		return getId();
	}

}
