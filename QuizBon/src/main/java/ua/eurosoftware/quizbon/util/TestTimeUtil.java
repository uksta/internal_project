package ua.eurosoftware.quizbon.util;

import java.time.LocalDateTime;

public class TestTimeUtil {
	
	private static final int PERMISSIBLE_INACCURACY = 5;
	
	public static boolean isQuizFinishInTime(LocalDateTime start, LocalDateTime end, int timePerQuestion, int maxQuestions){
		start.plusSeconds((timePerQuestion * maxQuestions) + PERMISSIBLE_INACCURACY);
		return end.isAfter(start);
	}

}
