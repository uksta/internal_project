package ua.eurosoftware.quizbon.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import ua.eurosoftware.quizbon.service.SettingsService;

public class PropertiesUtil {

	public static final String EXTENSION = ".properties";

	private static final Logger logger = Logger.getLogger(PropertiesUtil.class);
	private final Properties properties = new Properties();

	private PropertiesUtil(String fileFullName) {
		
		try (InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream(fileFullName + SettingsService.EXTENSION)) {
			if (in != null)
				properties.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static class Cache {
		private static Map<String, PropertiesUtil> holder = new HashMap<>();
	}

	public static String getProperty(PropertyType type, String fileName, String propKey) {
		String fileFullName = type.get() + fileName;

		return getProperty(fileFullName, propKey);
	}

	public static String getProperty(String fileFullName, String propKey) {
		PropertiesUtil propertiesCache = Cache.holder.get(fileFullName);

		logger.debug("Getting property with key " + propKey + " from file with full path " + fileFullName);

		if (propertiesCache == null) {
			propertiesCache = new PropertiesUtil(fileFullName);
			Cache.holder.put(fileFullName, propertiesCache);
		}

		return propertiesCache.properties.getProperty(propKey);
	}

	public static int getPropertyIntValue(PropertyType type, String fileName, String propKey) {
		return Integer.parseInt(getProperty(type, fileName, propKey));
	}

	public static int getPropertyIntValue(String fileFullName, String propKey) {
		return Integer.parseInt(getProperty(fileFullName, propKey));
	}

	public enum PropertyType {
		SQL("sql/"), SETTINGS("settings/");

		private String path;

		private PropertyType(String path) {
			this.path = path;
		}

		public String get() {
			return path;
		}
	}

}