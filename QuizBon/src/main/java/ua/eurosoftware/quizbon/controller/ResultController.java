package ua.eurosoftware.quizbon.controller;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ua.eurosoftware.quizbon.dto.Result;
import ua.eurosoftware.quizbon.dto.UserDetailedStatistic;
import ua.eurosoftware.quizbon.service.ResultService;
import ua.eurosoftware.quizbon.service.UserService;
import ua.eurosoftware.quizbon.util.Pagination;
import ua.eurosoftware.quizbon.util.PropertiesUtil;
import ua.eurosoftware.quizbon.util.PropertiesUtil.PropertyType;

@Controller
@RequestMapping(value = "/admin/quiz-results")
public class ResultController {

	private static final  Logger logger = Logger.getLogger(ResultController.class);
	
	@Inject
	private UserService userService;
	
	@Inject
	private ResultService resultService;

	private Long allUsersCount;
	
	@RequestMapping( method = RequestMethod.GET)
	public String openPage() {
		return "redirect:/admin/quiz-results/1";
	}

	@RequestMapping(value = "/{pageNumber}", method = RequestMethod.GET)
	public String openPage(@PathVariable int pageNumber, Model model) {
		model.addAttribute("title", "Results");
		if (allUsersCount == null) {
			allUsersCount = userService.getAllUsersCount();
		}
		int paginationCount = PropertiesUtil.getPropertyIntValue(PropertyType.SETTINGS, "test_config", "quiz.results.pagination.count");
		Pagination pagination = new Pagination(paginationCount, allUsersCount, pageNumber);
		
		model.addAttribute("location", "/admin/quiz-results");
        model.addAttribute("pagination", pagination);
        
		return "quiz-results";
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ResponseEntity<List<UserDetailedStatistic>> getAllUsers( @RequestParam("count") int count, @RequestParam("offset") int offset) {

		List<UserDetailedStatistic> userStatistic = userService.getAllUserStatistics(count, offset);

		return new ResponseEntity<>(userStatistic, HttpStatus.OK);
	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
	public ResponseEntity<Result> getResultByUserId(@PathVariable long id) {
		Result r = resultService.getResultByUserId(id);
		
		return new ResponseEntity<>(r, HttpStatus.OK);
	}
}
