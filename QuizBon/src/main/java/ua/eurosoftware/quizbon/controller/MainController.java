package ua.eurosoftware.quizbon.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ua.eurosoftware.quizbon.service.UserService;

@Controller
public class MainController {

	private static final Logger logger = Logger.getLogger(MainController.class);

	@Inject
	private UserService userService;

	@RequestMapping(value = { "/", "/home" })
	public String openHomePage(Model model) {
		model.addAttribute("title", "Java Quiz");
		return "home";
	}
	
	@RequestMapping(value="/test", method = RequestMethod.GET)
	public String showTestPage() {
		return "test";
	}

	@RequestMapping(value="/result")
	public String showTestResult() {
		return "result";
	}
	
	@RequestMapping(value = "/home", method = RequestMethod.GET, headers = "Accept=application/json")
	public  @ResponseBody Boolean verify(@RequestParam(value="email", required = false) String email,
			@RequestParam(value="phone", required=false) String phone) {
		if (email != null) {
			logger.debug("MainController.verify() email" + email);
			return !userService.isEmailExist(email.toLowerCase());
		}
		if (phone != null) {
			logger.debug("MainController.verify() phone " + phone);
			return !userService.isPhoneExist(phone);
		}
		return false;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLoginPage() {
		return "/admin/login";
	}
	
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String getErrorPage(HttpServletRequest request, HttpServletResponse response, Model model) {
		int errorCode = (int)request.getAttribute("javax.servlet.error.status_code");
		if(errorCode == 404) {
			String errorMessage = "Ops! Page not found :( ";
			setErrorAttributes("Page not found", errorMessage, model);
		} else {
			String errorMessage = "Ops! Something was going wrong :(";
			setErrorAttributes("Error", errorMessage, model);
		}
		model.addAttribute("errorCode", errorCode);
		return "error";
	}
	
	@RequestMapping(value = "/access_denied", method = RequestMethod.GET)
	public String accsesDenied(Model model) {
		String message = "Incorrect login data!";
		setErrorAttributes("Incorrect login data!", message, model);
		return "error";
	}
	
	private void setErrorAttributes(String title, String errorMessage, Model model) {
		model.addAttribute("errorTitle", title);
		model.addAttribute("errorMessage", errorMessage);
	}
}
