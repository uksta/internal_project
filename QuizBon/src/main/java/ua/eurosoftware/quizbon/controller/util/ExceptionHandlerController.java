package ua.eurosoftware.quizbon.controller.util;

import java.sql.SQLException;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController {
	
	@ExceptionHandler(value = SQLException.class)
	public String handleAllExceptions(){
		return "error-page";
	}

}
