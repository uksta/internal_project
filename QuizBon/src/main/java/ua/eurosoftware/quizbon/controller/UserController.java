package ua.eurosoftware.quizbon.controller;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ua.eurosoftware.quizbon.model.User;
import ua.eurosoftware.quizbon.service.UserService;

@Controller("/user")
public class UserController {

	private static final Logger logger = Logger.getLogger(UserController.class);

	@Inject
	private UserService userService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<User> save(@RequestBody User user) {

		logger.debug("UserController.save() " + user);
		userService.saveUser(user);
		return new ResponseEntity<>(user, HttpStatus.OK);

	}

}
