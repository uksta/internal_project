package ua.eurosoftware.quizbon.controller;

import java.time.LocalDateTime;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ua.eurosoftware.quizbon.dto.Quiz;
import ua.eurosoftware.quizbon.dto.Result;
import ua.eurosoftware.quizbon.model.User;
import ua.eurosoftware.quizbon.service.QuizService;
import ua.eurosoftware.quizbon.service.ResultService;
import ua.eurosoftware.quizbon.util.TestTimeUtil;

@Controller
@Scope("session")
@RequestMapping(value="/quiz")
public class QuizController {

	private static final Logger logger = Logger.getLogger(QuizController.class);
	
	@Inject
	private ResultService resultService;
	
	@Inject
	private QuizService questionService;

	private LocalDateTime quizStart;
	private LocalDateTime quizEnd;
	private int timePerQuestion;
	private int maxQuestions;


	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Quiz> getQuiz() {

		Quiz quiz = questionService.generateQuiz();
		timePerQuestion = quiz.getSecondsPerQuestion();
		maxQuestions = quiz.getTestQuestions().size();

		return new ResponseEntity<>(quiz, HttpStatus.OK);
	}

	@RequestMapping(value = "/start", method = RequestMethod.GET)
	public ResponseEntity<Void> startQuiz() {

		quizStart = LocalDateTime.now();
		logger.info("quiz start time is " + quizStart);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/finish", method = RequestMethod.GET)
	public ResponseEntity<Boolean> finishQuiz() {

		quizEnd = LocalDateTime.now();
		boolean correctFinish = TestTimeUtil.isQuizFinishInTime(quizStart, quizEnd, timePerQuestion, maxQuestions);
		logger.info("quiz end time is " + quizStart + ". Quiz end in time = " + correctFinish);

		return new ResponseEntity<>(correctFinish, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<User> save(@RequestBody Result result) {
		logger.debug("ResultController.save() " + result.getUser());
		
		result.getUserTestStatistic().setQuizTime(quizStart, quizEnd);
		resultService.saveResult(result);
		return new ResponseEntity<>(result.getUser(), HttpStatus.OK);

	}

}
