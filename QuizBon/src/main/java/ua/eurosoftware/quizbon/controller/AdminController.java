package ua.eurosoftware.quizbon.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ua.eurosoftware.quizbon.model.Answer;
import ua.eurosoftware.quizbon.model.Question;
import ua.eurosoftware.quizbon.model.QuizSettings;
import ua.eurosoftware.quizbon.service.QuizService;
import ua.eurosoftware.quizbon.service.SettingsService;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

	private static final int COUNT_QUESTIONS_ON_PAGE = 10;
	@Inject
	private QuizService quizService;

	@Inject
	private SettingsService settingsService;
	
	@RequestMapping(value = { "", "/", "/home" }, method = RequestMethod.GET)
	public String showhomePage(Model model, @RequestParam(required = false, defaultValue = "1") int page) {
		model.addAttribute("pages", getPageCount());
		model.addAttribute("activePage", page);
		return "/admin/admin_home";
	}

	@RequestMapping(value = "/remove", method = RequestMethod.GET)
	@ResponseBody
	public String remove(@RequestParam long id) {
		quizService.remove(id);
		return "success";
	}

	private int getOffsetByPageNumber(int pageNumber) {
		return pageNumber <= 1 ? 0 : (pageNumber - 1) * COUNT_QUESTIONS_ON_PAGE;
	}
	
	private int getPageCount() {
		int questionCount = quizService.getQuestionCount();
		if (questionCount % COUNT_QUESTIONS_ON_PAGE == 0) {
			return questionCount / COUNT_QUESTIONS_ON_PAGE;
		} else {
			return (questionCount / COUNT_QUESTIONS_ON_PAGE) + 1;
		}
	}
	
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public ResponseEntity<Question> add(@RequestBody Question question){
		quizService.add(question);
		return new ResponseEntity<>(question, HttpStatus.OK);
	}
	

	@RequestMapping(value="/question_list", method = RequestMethod.POST)
	public ResponseEntity<List<Question>> loadQuestionList(@RequestParam(required = false, defaultValue = "1") int page) {
		int offset = getOffsetByPageNumber(page);
		List<Question> questions = quizService.getPartByOffset(offset, COUNT_QUESTIONS_ON_PAGE);
		return new ResponseEntity<List<Question>>(questions, HttpStatus.OK);
	}
		
	@RequestMapping(value="/change", method = RequestMethod.POST)
	public ResponseEntity<Question> change(@RequestBody Question question){
		quizService.update(question);
		return new ResponseEntity<>(question, HttpStatus.OK);
	}

	@RequestMapping(value = "get_answers", method = RequestMethod.POST)
	public ResponseEntity<List<Answer>> getAnswersByQuestionId(@RequestParam long questionId) {
		List<Answer> answers = new ArrayList<>(0);
		if (questionId > 0) {
			answers = quizService.getAnswersByQuestionId(questionId);
		}
		return new ResponseEntity<List<Answer>>(answers, HttpStatus.OK);
	}

	@RequestMapping(value="/switch_question", method = RequestMethod.POST)
	@ResponseBody
	public String switchQuestionState(@RequestBody Question question) {
		quizService.switchQuestionState(question);
		return "success";
	}
	
	@RequestMapping(value = "/settings", method = RequestMethod.GET)
	public ResponseEntity<QuizSettings> getPropertyFileTestTime() {
		QuizSettings settings = settingsService.getSettings();
		return new ResponseEntity<>(settings, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/settings_questions_count", method = RequestMethod.GET)
	public ResponseEntity<Integer> getQuestionsCount() {
		int questionsCount = quizService.getQuestionCount();
		return new ResponseEntity<>(questionsCount, HttpStatus.OK);
	}

	@RequestMapping(value = "/update_settings", method = RequestMethod.POST)
	public ResponseEntity<Void> updateSettings(@RequestBody QuizSettings quizSettings) {
		
		settingsService.updatePropertyFile(quizSettings);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/pagination", method = RequestMethod.POST)
	public ResponseEntity<Map<String,Integer>> getPaginationData() {
		Map<String, Integer> map = new HashMap<>();
		map.put("pageCount", getPageCount());
		return new ResponseEntity<>(map, HttpStatus.OK);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}