package ua.eurosoftware.quizbon.listeners;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

import ua.eurosoftware.quizbon.util.PropertiesUtil;
import ua.eurosoftware.quizbon.util.PropertiesUtil.PropertyType;

public class InitContextListener implements ServletContextListener{

	private static final Logger logger = Logger.getLogger(InitContextListener.class);
	 
	protected WebApplicationContext getWebApplicationContext(ServletContextEvent event) {
		ServletContext servletContext = event.getServletContext();
		
		return (WebApplicationContext) servletContext.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.info("QuizDom has been destroyed");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ServletContext context = arg0.getServletContext();
        String contextPath = context.getContextPath();
        context.setAttribute("context", contextPath);
        
        context.setAttribute("CSS_JS_VERSION", PropertiesUtil.getProperty(PropertyType.SETTINGS, "test_config", "cssJsVersion"));
        context.setAttribute("productionMode", PropertiesUtil.getProperty(PropertyType.SETTINGS, "test_config", "production.mode"));
        
        logger.info("QuizDom has been started");
	}

}
