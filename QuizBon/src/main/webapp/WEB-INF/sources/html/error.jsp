<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>${errorTitle}</title>
 <meta name="decorator" content="no" />
<link rel="stylesheet" type="text/css" href="resources/css/error.css?v=${CSS_JS_VERSION}">
</head>
<body>
<span id="error_message">${errorMessage}</span>
</body>
</html>