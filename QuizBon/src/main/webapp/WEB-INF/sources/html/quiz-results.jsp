<%@ page language="java" contentType="text/html; charset=UTF8" pageEncoding="UTF8"%>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script>
	var offset = '${pagination.offset}';
	var count = '${pagination.count}';
</script>

<div class="quiz-result-content">
	<div id='quiz'></div>
	<table id="quiz-table" class="table table-hover">
		<thead>
			<tr>
				<th class="col-md-3 col-sm-3">Full Name</th>
				<th class="col-md-3 col-sm-3">Email</th>
				<th class="col-md-3 col-sm-3">Phone</th>
				<th class="col-md-2 col-sm-2">Skype</th>
				<th class="col-md-1 col-sm-1">Result</th>
			</tr>
		</thead>
		<tbody id="users-results">
		</tbody>
	</table>
</div>
<div id="pagination-box">
	<a href="<c:url value='/admin'/>">
		<button class="btn-func" id="back-to-admin-btn">back to admin</button>
	</a>
	<jsp:include page="templates/pagination.jsp" />
</div>

<c:choose>
    <c:when test="${productionMode}">
		<script src="${context}/resources/js/jquery-2.2.2.min.js?v=${CSS_JS_VERSION}"></script>
		<script src="${context}/resources/js/quiz-results.min.js?v=${CSS_JS_VERSION}"></script>
    </c:when>
    <c:otherwise>
		<script src="${context}/resources/js/jquery-2.2.2.min.js?v=${CSS_JS_VERSION}"></script>
		<script src="${context}/resources/js/quiz-results.js?v=${CSS_JS_VERSION}"></script>
    </c:otherwise>
</c:choose>

