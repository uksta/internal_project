<%@ page language="java" contentType="text/html; charset=UTF8" pageEncoding="UTF8"%>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="user-wrapper">
		<div class="col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
			<form id="user-id-form" class="form-horizontal" method="POST">
				<div class="form-group">
					<label for="first-name" class="col-xs-4 col-sm-3 col-md-3 col-lg-4 control-label">First name:<span id="required">*</span></label>
					<div class="col-xs-8 col-sm-9 col-md-9 col-lg-8">
						<input id="first-name" class="form-control" autocomplete="off" name="firstname" />
					</div>
				</div>
				<div class="form-group">
					<label for="last-name" class="col-xs-4 col-sm-3 col-md-3 col-lg-4 control-label">Last name:<span id="required">*</span></label>
					<div class="col-xs-8 col-sm-9 col-md-9 col-lg-8">
						<input id="last-name" class="form-control" autocomplete="off" name="lastname" />
					</div>
				</div>
				<div class="form-group">
					<label for="user-phone" class="col-xs-4 col-sm-3 col-md-3 col-lg-4 control-label">Phone:<span id="required">*</span></label>
					<div class="col-xs-8 col-sm-9 col-md-9 col-lg-8">
						<input id="user-phone" type="text" class="input-medium bfh-phone form-control" autocomplete="off" placeholder="e.g: 050 050-50-50" data-format="ddd ddd-dd-dd" name="phone" />
					</div>
				</div>
				<div class="form-group">
					<label for="user-email" class="col-xs-4 col-sm-3 col-md-3 col-lg-4 control-label">Email:<span id="required">*</span></label>
					<div class="col-xs-8 col-sm-9 col-md-9 col-lg-8">
						<input id="user-email" placeholder="e.g: email@mail.com" autocomplete="off" class="form-control" name="email" />
					</div>
				</div>
				<div class="form-group">
					<label for="user-skype" class="col-xs-4 col-sm-3 col-md-3 col-lg-4 control-label">Skype:</label>
					<div class="col-xs-8 col-sm-9 col-md-9 col-lg-8">
						<input id="user-skype" class="form-control" autocomplete="off" name="skype" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-offset-4 col-xs-8 col-sm-offset-3 col-sm-9 col-md-offset-3 col-md-9 col-lg-offset-4 col-lg-8">
						<button type="submit" class="btn-func">Start</button>
					</div>
				</div>
			</form>
		</div>
</div>

<div id="rules-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="welcome-message" class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button id="start-quiz" type="button" class="btn btn-primary">OK</button>
      </div>
    </div>
  </div>
</div>

<div id="quiz-content" hidden="true">
	<jsp:include page="quiz.jsp"></jsp:include>
</div>

<div id="result-content" hidden="true">
	<jsp:include page="result.jsp"></jsp:include>
</div>

<c:choose>
    <c:when test="${productionMode}">
        <script src="${context}/resources/js/quiz.min.js?v=${CSS_JS_VERSION}"></script>
		<script src="${context}/resources/js/validation.min.js?v=${CSS_JS_VERSION}"></script>
    </c:when>
    <c:otherwise>
       	<script src="${context}/resources/js/quiz.js?v=${CSS_JS_VERSION}"></script>
		<script src="${context}/resources/js/validation.js?v=${CSS_JS_VERSION}"></script>
    </c:otherwise>
</c:choose>
