<%@ page language="java" contentType="text/html; charset=UTF8"
	pageEncoding="UTF8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator"%>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 "> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8 br-ie7"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9 br-ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
<link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<c:choose>
    <c:when test="${productionMode}">
        <link rel="stylesheet" href="${context }/resources/css/styles.min.css?v=${CSS_JS_VERSION}" />
    </c:when>
    <c:otherwise>
        <link rel="stylesheet" href="${context }/resources/css/styles.css?v=${CSS_JS_VERSION}" />
    </c:otherwise>
</c:choose>


	<script src="${context}/resources/js/jquery-2.2.2.min.js"></script>
	<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="${context}/resources/js/jquery.form.min.js"></script>
	<script src="${context}/resources/js/jquery.validate.min.js"></script>
	<script src="${context}/resources/js/bootstrap-formhelpers-phone.min.js"></script>

<title>Quiz Dom</title>
</head>
<body>

	<header>
		<div id="logo" class="col-xs-12 col-sm-5 col-md-3">
			<img alt="Eurosoftware" src="${context}/resources/images/logo.png" />
		</div>
		<div class="col-xs-12 col-sm-7 col-md-6" id="welcome-text">
			<p>${title}</p>
		</div>
	</header>

	<div id="wrapper">
		<section id="content">
			<decorator:body />
		</section>
	</div>

	<footer>
		<div class="col-xs-offset-2 col-xs-8 col-sm-offset-3 col-sm-6 col-md-offset-4 col-md-4">
			<p>&copy; 2016 Eurosoftware, Lviv</p>
		</div>
	</footer>
</body>
</html>