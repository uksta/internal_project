<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="decorator"
	uri="http://www.opensymphony.com/sitemesh/decorator"%>
<div id='timer'></div>

<div id='quiz'>
	<section id="quiz-wrapper"></section>
</div>
<div class="col-xs-offset-4 col-xs-8 col-sm-offset-4 col-sm-8 col-md-offset-6 col-md-3">
	<button id="finish-test" class="btn-func">Finish test</button>
</div>

