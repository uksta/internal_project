<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Admin home</title>
        <meta name="decorator" content="no" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/admin_question_pop_up.css?v=${CSS_JS_VERSION}"/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/settings_pop_up.css?v=${CSS_JS_VERSION}"/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/admin_home.css?v=${CSS_JS_VERSION}"/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/custom_elements.css?v=${CSS_JS_VERSION}"/>"/>
        <script	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="<c:url value="/resources/js/jquery-2.2.2.min.js"/>"></script>
        <script src="<c:url value="/resources/js/admin/admin_home.js?v=${CSS_JS_VERSION}"/>"></script>
    </head>

    <body>
        <jsp:include page="admin_header.jsp"/>
        <div class="container">

            <!--  ADD question POPUP window -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-content">
                    <div id="add_question_pop_up" class="add_question_pop_up">
						<input id='qId' type='text' style='display:none;'/>
                        <a id="btnClose" class='close-dialog' data-dismiss="modal"></a>

	 	                <span class="title"> ADD NEW QUESTION </span>

                        <div class="text_block question_text_block">
                            <span class="field_title">Question:</span>
                            <textarea id="question_text_field" class="question_text_field" type="text" name="text"></textarea>
                        </div>


                        <div id="answers_fields">
                            <div class="text_block answer_text_block">
                                <span class="field_title">Answer 1:</span>
                                <input class="answer_text_field" type="text"/>
                                <input class="chbox is_correct" type="checkbox"/>
                                <label class='is_correct'> <input class='chbox' type='checkbox' name='vehicle' disabled readonly> correct </label>
                            </div>

                            <div class="text_block answer_text_block">
                                <span class="field_title">Answer 2:</span>
                                <input class="answer_text_field" type="text"/>
                                <input class="chbox is_correct" type="checkbox"/>
                            </div>
                        </div> 
                        <img class="add_answer_button" src="${context}/resources/images/plus_answer.png"/>
                        <button id="save_button" class="myButton save_button" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>

<!--  SETTINGS POPUP window -->
		<div class="modal fade" id="myModalSettings" role="dialog">
			<div class="modal-content">
				<div class="settings_pop_up">
						
						<a id="btnClose" class='close-dialog' data-dismiss="modal"></a>
						
						<span class="settings_title">SETTINGS </span>
						
 						<div class="settings_text_block">
									<span class="settings_field_title">General quiz test time (in seconds):</span>
						<input type="number" min="1" step="1" id="settings_time" class="settings_time_text_field" name="settings_field" /> 
						</div> 
						
						<div class="settings_text_block">
									<span class="settings_field_title">General questions number:</span>
						<input type="number" min="1" step="1" id="settings_count" class="settings_time_text_field" name="settings_field" />
						</div> 
						<div class="center-align">
							<button id="settings_save_button" class="myButton settings_save_button">Save</button>
							<button class="myButton settings_save_button" data-dismiss="modal">Cancel</button>
						</div>
				</div>
			</div>
		</div>
        </div>
        <div id="content">
            <div id="questions_tools_panel">
                <button id="add_question_button" type="button" class="myButton" data-toggle="modal" data-target="#myModal"> Add question </button>
            </div>
            <div id="question_list">
            </div>

            <div id="page_panel">
                
            </div>
        </div>


    </body>
</html>