<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
		<div id="header">
            <ul>
                <li>
                    <a class="header_link" href="<c:url value="/logout"/>">
                        <img class="menu_icon" src="${context}/resources/images/logout.png" /> Logout
                    </a>
                </li>
			<li>
				<a id="mySettings" class="header_link" data-toggle="modal" data-target="#myModalSettings"> 
					<img class="menu_icon" src="${context}/resources/images/settings.png" /> Settings
				</a>
			</li>
			<li>
				<a id="test_results" class="header_link" href="<c:url value="/admin/quiz-results"/>"> 
					<img class="menu_icon" src="${context}/resources/images/test_results.png" /> Test results
				</a>
			</li>
            </ul>
        </div>