<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="decorator" content="no" />
<title>Sign in</title>
<link rel="stylesheet" type="text/css" href="resources/css/custom_elements.css?v=${CSS_JS_VERSION}">
<script type="text/javascript">
	function setFocus() {
		document.getElementById("username").focus();
	}
</script>
<style>
body {
	margin: 0 auto;
	font: 14px Arial;
	background: #fafafd;
}

form {
    height: 160px;
	width: 380px;
	border: 1px solid #B4B7B4;
	border-radius: 4px;
	box-shadow: 1px 1px 18px rgba(0, 0, 0, 0.3);
	padding: 14px;
	position: relative;
	margin: 5px auto;
	top: 120px;
	background: #ECEFED;
}

table {
	width: 100%;
	height: 55%;
	position: relative;
	top: 8px;
}

.myInput {
	border: 1px solid #cecece;
	height: 22px;
	width: 200px;
	border-radius: 4px;
	padding: 2px;
}


</style>
</head>
<body onload="setFocus();">
	<form action="<c:url value="/sign_in"/>" method="post">
		<table>
			<tr>
				<td colspan="2"
					style="padding-bottom: 12px; text-align: center; font: 16px Arial;">
					Sign in system, please!</td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><input class="myInput" id="username" type="text"
					name="j_username" /></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input class="myInput" id="password" type="password"
					name="j_password" /></td>
			</tr>
			<tr>
				<td style="padding-top: 16px;"><input class="myButton"
					type="submit" value="Enter" /></td>
			</tr>
		</table>
	</form>
</body>
</html>