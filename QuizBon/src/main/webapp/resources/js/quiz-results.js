var components = {
	userResultId : '#users-results',
	quizWrapperId : "#quiz-wrapper",
	quizResultContentClass : ".quiz-result-content",
	quizTableId : "#quiz-table",
	quizId : '#quiz',
	paginationBoxId : '#pagination-box'
};

$(function getUsers() {
	$.ajax({
		url : "users",
		data : {count : count, offset : offset},
		type : 'GET',
		success : function (data) {
			var content = $(components.userResultId);
			for (var i in data) {
				content.append(buildUserRow(data[i].user, data[i].userTestStatistic));
			}
			$(components.paginationBoxId).attr("hidden", false);
		}
	});
});

function buildUserRow(user, userTestStatistic){
	var row = $('<tr>', {
		user : user.id
	});
	row.click(function(){
		getQuiz($(this).attr('user'));
	});
	var fullnameCol = $('<td>',{ text: user.firstName + " " + user.lastName, 'class' : "name"}).appendTo(row);
	var emailCol = $('<td>',{ text: user.email}).appendTo(row);
	var phoneCol = $('<td>',{ text: user.phone}).appendTo(row);
	var skypeCol = $('<td>',{ text: user.skype}).appendTo(row);
	var resultCol = $('<td>',{ text: userTestStatistic.result + " of " + userTestStatistic.testMax}).appendTo(row);
	return row;
}


function getQuiz(userId) {
	var quizContent = $(components.quizId);
	$.ajax({
		url : "users/" + userId,
		type : 'GET',
		contentType : "application/json",
		success : function(data) {
			var header = buildQuizHeader(userId);
			$(components.quizTableId).remove();
			quizContent.html(header);
			quizContent.append(buildQuestions(data.questions));
			var backBtn = $('<button>',{'class' : "btn-func", id : "back-btn"}).text("back").appendTo($(components.quizResultContentClass));
			backBtn.click(function(){
				location.reload();
			});
			
			$(components.paginationBoxId).attr("hidden", true);
		}
	});
}

function buildQuizHeader(userId){
	var div = $('<div>',{id : "quiz-result-header"});
	var fullName = $('tr[user="' + userId + '"]').find('td.name').html();
	var label = $('<label>',{text : 'Results of: ' + fullName}).appendTo(div);
	return div;
}

function buildQuestions(data) {
	var questionsSection = $('<section id="quiz-wrapper"></section>');
	
	for ( var i in data) {
		var question = createQuestion(data[i], i);
		questionsSection.append(question);
	}
	return questionsSection;
}

function createQuestion(question, index) {

	var parentDiv = $('<div class="col-md-offset-3 col-md-6"></div>');
	var wrapperDiv = $('<div class="panel panel-default"></div>');
	var questionTitleDiv = $('<div class="panel panel-heading"></div>')
	var answers = buildAnswers(question.answers);
	parentDiv.append(wrapperDiv.append(questionTitleDiv.html((parseInt(index) + 1) + ". " + question.text)).append(answers));
	return parentDiv;
}

function buildAnswers(answers) {

	var answersPanelDiv = $('<div class="panel-body">');
	var ul = $('<ul></ul>');

	for ( var i in answers) {
		var li = $('<li>',{'class'  :"answer"});
		var div = $('<div>',{'class' : "checkbox"});
		var label = $('<label></label>');
		var checkBox = $('<input>', {'class' : "checkbox-default", type : "checkbox", disabled : "true" });
		var isChecked = answers[i].checked;
		checkBox.attr('checked', isChecked);
		if(isChecked && !answers[i].correct)  label.addClass("wrong");
		if(answers[i].correct) label.addClass("right");
		li.append(div.append(label.append(checkBox).append(answers[i].text)));
		ul.append(li);
	}
	return answersPanelDiv.append(ul);
}