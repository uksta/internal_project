$(document).ready(function () {
    $('#user-id-form').validate({
        focusCleanup: true,
        focusInvalid: false,
        onkeyup: false,
        rules: {
            firstname: {
                required: true
            },
            lastname: {
                required: true
            },
            phone: {
                required: true,
                minlength: 13,
                remote: "home"
            },
            email: {
                required: true,
                email: true,
                remote: "home"
            }
        },
        messages: {
            firstname: {
                required: 'First name is required and cannot be empty'
            },
            lastname: {
                required: 'Last name is required and cannot be empty'
            },
            phone: {
                required: 'Phone is required and cannot be empty',
                minlength: 'Please enter valid phone number',
                remote: 'Current phone is already registered'
            },
            email: {
                required: 'Email is required and cannot be empty',
                email: 'Please enter valid email address',
                remote: 'Current email is already registered'
            }
        },
        submitHandler: function() { 
       		$(components.rulesModalId).modal('toggle');
        }
    });
});