var maxQuestions = 0;

$(document).ready(function () {
    $("#question_list").ready(function () {
        var currentPage = getCurrentPage();
        if (currentPage == null) {
            currentPage = 1;
        }
        $.ajax({
            url: "admin/question_list?page=" + currentPage,
            type: 'POST',
            contentType: "application/json",
            success: function (result) {
                for (var i in result) {
                    var mainDiv = $("<div id='" + result[i].id + "' class='question'></div>");
                    var questionText = $("<span class='question_text'>" + result[i].text + "</span>");
                    var buttonsDiv = $("<div class='question_buttons'></div>");
                    var changeButton = $("<button class='myButton change_question' data-toggle='modal' data-target='#myModal'>Change</button>")
                    var isEnabledCheckbox;
                    if( result[i].enabled){
                    	isEnabledCheckbox = $("<input class='is_active_question' type='checkbox' title='isEnabled' checked/>")
                    } else {
                    	isEnabledCheckbox = $("<input class='is_active_question' type='checkbox' title='isEnabled'/>")
                    }
                    changeButton.click(function () {
                    	$("#question_text_field").prop("disabled", true);
                        changeQuestion($(this));
                    });
                    isEnabledCheckbox.click(function() {
                    	switchQuestionState($(this).is(":checked"), $(this).parent().parent().attr("id"));
                    });

                    var questionBlock = mainDiv.append(questionText).append(buttonsDiv.append(changeButton).append(isEnabledCheckbox));
                    $("#question_list").append(questionBlock);
                }
            }
        });
    });


    function getCurrentPage() {
        var url = window.location.search.substring(1);
        var params = url.split('&');
        for (var i = 0; i < params.length; i++) {
            var paramName = params[i].split('=');
            if (paramName[0] == 'page') {
                return paramName[1];
            }
        }
    }

    $("#save_button").click(function(){
      	saveQuestion();
    });
    
    function saveQuestion() {
        var question = $("#question_text_field").val();
        var answers = $(".answer_text_block");
        var answersJson = [];
        $.each(answers, function (index, value) {
            var correct = $(value).find(".is_correct").find("input").is(":checked");
            var active = $(value).find(".is_active").find("input").is(":checked");
            var answer = {
            		"id": $(value).attr("name"),
            		"text": $(value).find(".answer_text_field").val(),
            		"correct": correct,
            		"active": active
            		};
            answersJson.push(answer);
        });

        var link;
        if ($("#save_button").hasClass("change_data")) {
        	link = 'admin/change';
            $("#save_button").removeClass("change_data");
        } else {
        	link = 'admin/add';
        }
        
        var questionJson = {
            "id": $("#qId").val(),
        	"text": question,
            "answers": answersJson
        };

        $.ajax({
            url: link,
            type: 'POST',
            data: JSON.stringify(questionJson),
            contentType: "application/json",
            success: function (result) {
            	resetPopupState();
            }
        });
    }

    $(".add_answer_button").click(function () {
        var answerFields = $("#answers_fields").children();
        var count = answerFields.length + 1;

        var newAnswerBlock = $("<div class='answer_text_block'></div>");
        var span = $("<span class='field_title'></span>").append("Answer " + count + ":").appendTo(newAnswerBlock);
        var textField = $("<input class='answer_text_field' type='text' />").appendTo(newAnswerBlock);

        newAnswerBlock.addClass('text_block');
        var delBtn = $("<button class='del_btn'></button>").append("X").appendTo(newAnswerBlock);
        $("<label class='is_correct'> <input class='chbox new_answer' type='checkbox' name='vehicle'> correct </label>").appendTo(newAnswerBlock);
        $("<label class='is_active'> <input class='chbox new_answer' type='checkbox'> active </label>").appendTo(newAnswerBlock);
        delBtn.click(function () {
            delblock.call(this);
        });

        $("#answers_fields").append(newAnswerBlock);
    });

    function addAnswerBlockOnChange(answer) {
        var answerFields = $("#answers_fields").children();
        var count = answerFields.length + 1;
        var newAnswerBlock = $("<div class='answer_text_block' name='" + answer.id + "'></div>");
        var span = $("<span class='field_title'></span>").append("Answer " + count + ":").appendTo(newAnswerBlock);
        var textField = $("<input class='answer_text_field' type='text' disabled/>").css("background", "#d5d5d5");
        
        textField.val(answer.text);
        textField.appendTo(newAnswerBlock);
        newAnswerBlock.addClass('text_block');

        var isCorrectCheckBox;
        if (answer.correct) {
        	isCorrectCheckBox = $("<label class='is_correct'> <input class='chbox' type='checkbox' name='vehicle' checked disabled readonly> correct </label>");
        } else {
        	isCorrectCheckBox = $("<label class='is_correct'> <input class='chbox' type='checkbox' name='vehicle' disabled readonly> correct </label>");
        }
        
        var isActiveCheckBox;
        if (answer.active) {
        	isActiveCheckBox = $("<label class='is_active'> <input class='chbox' type='checkbox' checked> active </label>");
        } else {
        	isActiveCheckBox = $("<label class='is_active'> <input class='chbox' type='checkbox'> active </label>");
        }
        
        isCorrectCheckBox.appendTo(newAnswerBlock);
        isActiveCheckBox.appendTo(newAnswerBlock);
        $("#answers_fields").append(newAnswerBlock);
    }

    function delblock() {
        var choosedAnswer = $(this).parent();
        choosedAnswer.remove()
    }

    $(".del_btn").click(function () {
        delblock.call(this);
    });

    function changeQuestion(thisItem) {
        var questionBlock = thisItem.parent().parent();
        var id = questionBlock.attr("id");
        var question = questionBlock.find(".question_text").text();
        $("#question_text_field").val(question);
        $("#question_text_field").attr("disabled","disabled");
        $("#question_text_field").css("background","#d5d5d5");
        getQuestionAnswers(id);
        $("#save_button").addClass("change_data")
        $("#qId").val(id);
    }
    
    function deleteExsistAnswerBlocks() {
        $("#answers_fields").empty();
    }

    function getQuestionAnswers(questionId) {
        $.ajax({
            url: "admin/get_answers?questionId=" + questionId,
            method: 'POST',
            contentType: 'application/json',
            success: function (data) {
                if (data.length > 0) {
                    deleteExsistAnswerBlocks();
                }
                for (var i in data) {
                    addAnswerBlockOnChange(data[i]);
                }
            }
        });
    }
    
    function switchQuestionState(isEnabled, questionId) {
    	$.ajax({
			url: "admin/switch_question",
			method: 'POST',
			data: JSON.stringify({"id": questionId, "enabled": isEnabled}),
			contentType: "application/json",
			success: function(result) {

			}
		});
    }
    
 	$("#mySettings").click(function(){
 		$.ajax({
 			url: "admin/settings_questions_count",
 			method: 'GET',
 			success: function(result) {
 				maxQuestions = result;
 			}
 		});
		$.ajax({
			url: "admin/settings",
			method: 'GET',
			success: function(result) {
				$("#settings_time").val(result.testTime);
		 		$("#settings_count").val(result.questionsCount);
			}
		});
 	});

    function resetPopupState() {
    	deleteExsistAnswerBlocks();
    	addNecessaryAnswerFields(2);
    	$("#question_text_field").val("");
    }

    function addNecessaryAnswerFields(fieldsCount) {
    	var answerFields = $("#answers_fields").children();
     
        for(var i=0; i<fieldsCount; i++) {
        	var count = answerFields.length + 1;
	        var newAnswerBlock = $("<div class='answer_text_block'></div>");
	        var span = $("<span class='field_title'></span>").append("Answer " + count + ":").appendTo(newAnswerBlock);
	        var textField = $("<input class='answer_text_field' type='text' />").appendTo(newAnswerBlock);

	        newAnswerBlock.addClass('text_block');
	        var delBtn = $("<button class='del_btn'></button>").append("X").appendTo(newAnswerBlock);
	        $("<label class='is_correct'> <input class='chbox' type='checkbox' name='vehicle'> correct </label>").appendTo(newAnswerBlock);
	        $("<label class='is_active'> <input class='chbox' type='checkbox'> active </label>").appendTo(newAnswerBlock);
	        delBtn.click(function () {
	            delblock.call(this)
	        });
	
	        $("#answers_fields").append(newAnswerBlock);
        }
    }
    
    $("#btnClose").click(function(){
    	resetPopupState();
    	$("#save_button").removeClass("change_data");
    });

	$("#add_question_button").click(function() {
		$("#question_text_field").prop("disabled", false);
		$("#question_text_field").css("background", "#ffffff");
		resetPopupState();
	});
	
	$("#settings_save_button").click(function(){
		
		var timeInput = $("#settings_time");
		var countInput = $("#settings_count");
		
		var isTimeValid = validateNumber.call(timeInput,timeInput.val());
		var isCountValid = validateNumber.call(countInput,countInput.val());
		
		var settingsJson = {
			"testTime": timeInput.val(),
			"questionsCount": countInput.val()
		};
		
		if(isTimeValid && isCountValid){
			$.ajax({
				url: 'admin/update_settings',
				type : 'POST',
				data : JSON.stringify(settingsJson),
				contentType : "application/json",
				success : function(result) {
		           	console.log(" Settings were saved...");
				}
			});
			$(this).attr('data-dismiss',"modal");
			$(this).click();
		}else{
			alert("input valid numbers");
		}
	});
	
	$('#settings_time').focusout(function(){
		validateNumber.call(this, $(this).val());
	});
	
	$('#settings_count').focusout(function(){
		validateQuizCount.call(this, $(this).val());
	});
	
	$('#settings_time').keypress(function(){
		validateNumber.call(this, $(this).val());
	});
	
	$('#settings_count').keypress(function(){
		validateQuizCount.call(this, $(this).val());
	});
	
	function validateNumber(value){
		var regex = /^\+?([1-9]\d*)$/;
		if(!regex.test(value)){
			$(this).val('');
			return false;
		}
		return true;
	}
	
	function validateQuizCount(value){
		var isValidNum = validateNumber.call(this, value);
		
		if(isValidNum && (value <= maxQuestions)){
			return true;
		}
		$(this).val('');
		return false;
	}

	
	// SCARIED PAGINATOR 
	
	$("#page_panel").ready(function() {
		$.ajax({
			url: 'admin/pagination',
			type: 'POST',
			contentType: "application/json",
			success: function(result) {
				var pageCount = result.pageCount;
				var activePage = getCurrentPage();
		        if (activePage == null) {
		        	activePage = 1;
		        }
				var maxPages = 10;
				var currentPage = activePage;
				
				if(activePage > 1) {
					if(activePage > 3) {
						for (var i = 1; i <= 3; i++) {
							$("<a class='page_link' href='admin?page=" + i + "'> " + i + " </a>").appendTo("#page_panel");
						}
						$("<span> ... <span>").appendTo("#page_panel");
					} else {
						$("<a class='page_link' href='admin?page=" + 1 + "'> " + 1 + " </a><span> ... <span>").appendTo("#page_panel");
					}
				}
				for (var i=0; i<maxPages; i++) {
					if (currentPage <= pageCount) {
						if(i < maxPages-1) {
							if(currentPage != activePage) {
								$("<a class='page_link' href='admin?page=" + currentPage + "'> " + currentPage + " </a>").appendTo("#page_panel");
							} else {
								$("<a style='color: red;' href='#'> " + currentPage + " </a>").appendTo("#page_panel");
							}
						} else {
							$("<span> ... <span> <a class='page_link' href='admin?page=" + pageCount + "'> " + pageCount + " </a>").appendTo("#page_panel");
						}
					}
					currentPage++;
				}
			}
		});		
	});
});

