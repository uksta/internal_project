var components = {
	startQuizId : "#start-quiz",
	quizContentId : "#quiz-content",
	quizWrapperId : "#quiz-wrapper",
	userFormWrapperId : '#user-wrapper',
	resultContentId : '#result-content',
	quizId : "#quiz",
	finishTestId : "#finish-test",
	timerId : '#timer',
	firstNameId : '#first-name',
	lastNameId : '#last-name',
	userPhoneId : '#user-phone',
	userEmailId : '#user-email',
	userSkypeId : '#user-skype',
	congratMessageId : '#congratMessage',
	closeResultId : "#close-result",
	welcomeMessageId : "#welcome-message",
	rulesModalId : "#rules-modal",
	resultModalId : "#result-modal"
};

var messages = {
	unsavedSession : "Are you sure want to reload this page? Test data will be lost"
};

var questions = [];
var countDownTimer;
var secondsPerQuestion;

window.onbeforeunload = function() {
	if ($(components.quizContentId).is(":hidden") !== true)
		return messages.unsavedSession;
};

$(function() {

	function reposition() {
        var modal = $(this),
            dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');

        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    }
	getQuiz();
    $('.modal').on('show.bs.modal', reposition);

    $(window).on('resize', function() {
        $('.modal:visible').each(reposition);
    });

});

function getQuiz() {
	var quizContent = $(components.quizId);
	$.ajax({
		url : "quiz",
		type : 'GET',
		contentType : "application/json",
		success : function(data) {
			quizContent.html(buildQuestions(data.testQuestions));
			secondsPerQuestion = data.secondsPerQuestion;
			buildWelcomeRules(data.testQuestions.length, data.secondsPerQuestion);
		}
	});
}

$(components.startQuizId).click(function(){
	$(components.rulesModalId).modal('hide');
	$.ajax({
		url : "quiz/start",
		type : 'GET',
		success : function() {
			showQuizHideUserForm();
			startTimer();
		}
	});
});

function buildWelcomeRules(questionsAmount, secondsPerQuestion){
	if(questionsAmount != typeof 'undefined' && this.secondsPerQuestion != typeof 'undefined'){
		$(components.welcomeMessageId).html("You will have " + questionsAmount + " questions and " + getWelcomeTime() + ". Good luck!");
	}
	function getWelcomeTime(){
		var minutes = parseInt((this.secondsPerQuestion * questions.length) / 60);
		var seconds = (this.secondsPerQuestion * questions.length) % 60;
		var time = minutes + " min " + (seconds  < 10 ? "0" + seconds : seconds) + " sec";
		return time;
	}
}


function buildQuestions(data) {
	questions = data;
	var questionsSection = $(components.quizWrapperId);
	questionsSection.html('');
	for ( var i in data) {
		var question = createQuestion(data[i], i);
		questionsSection.append(question);
	}
	return questionsSection;
}

function createQuestion(question, index) {

	var parentDiv = $('<div class="col-xs-12 col-sm-offset-2 col-sm-10 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6"></div>');
	var wrapperDiv = $('<div class="panel panel-default"></div>');
	var questionTitleDiv = $('<div class="panel panel-heading" id="question-text" unselectable="on" onselectstart="return false;" onmousedown="return false;"></div>');
	var answers = buildAnswers(question.answers);
	parentDiv.append(wrapperDiv.append(questionTitleDiv.html((parseInt(index) + 1) + ". " + question.text))
			.append(answers));
	return parentDiv;

}

function buildAnswers(answers) {

	var answersPanelDiv = $('<div class="panel-body">');
	var ul = $('<ul></ul>');

	for ( var i in answers) {
		var li = $('<li class="answer"></li>');
		var div = $('<div class="checkbox"></div>');
		var label = $('<label></label>');
		var checkBox = $('<input id="que" class="checkbox-default" type="checkbox" question="' +
		answers[i].questionId + '" answer="' + answers[i].id + '"/>');
		li.append(div.append(label.append(checkBox).append(answers[i].text)));
		ul.append(li);
	}
	return answersPanelDiv.append(ul);
}

function startTimer() {
	var duration = secondsPerQuestion;
	var start = Date.now(), diff, minutes, seconds;
	duration = duration * questions.length;
	var timerDiv = $('#timer');
	function timer() {
		diff = duration - (((Date.now() - start) / 1000) | 0);

		minutes = (diff / 60) | 0;
		seconds = (diff % 60) | 0;

		minutes = minutes < 10 ? "0" + minutes : minutes;
		seconds = seconds < 10 ? "0" + seconds : seconds;

		timerDiv.text(minutes + ":" + seconds);

		if (diff <= 0) {
			clearTimeout(countDownTimer);
			$(components.finishTestId).click();
		}
	}
	timer();
	countDownTimer = setInterval(timer, 1000);
}

function showQuizHideUserForm() {
	$(components.quizContentId).attr("hidden", false);
	$(components.userFormWrapperId).attr("hidden", true);
}

function showResultHideQuiz() {
	$(components.resultContentId).attr("hidden", false);
	$(components.quizContentId).attr("hidden", true);
}
$(components.finishTestId).click(function() {
	finishTest();
});

function finishTest(){
	$.ajax({
		url : "quiz/finish",
		type : 'GET',
		success : function(isValid) {
			if(isValid){
				var testResult = getTotalResults();
				saveResult(testResult);
				showResultHideQuiz();
				var message = 'Dear, ' + $(components.firstNameId).val() +
							  '! You have answered right on ' + testResult + ' ' +
							  (testResult > 1 ? "questions" : "question") + ' of ' +
							  questions.length;
				$(components.congratMessageId).html(message);
				$(components.resultModalId).modal();
			}else{
				alert("SHIT");
			}
		}
	});
}

function saveResult(testResult){
	var json = {
		user : {
			firstName : $(components.firstNameId).val(),
			lastName : $(components.lastNameId).val(),
			email : $(components.userEmailId).val(),
			phone : $(components.userPhoneId).val(),
			skype : $(components.userSkypeId).val()			
		},
		userTestStatistic : {
			result : testResult,
			testMax : questions.length
		},
		questions : getQuizResults()
	};
	$.ajax({
		url : "quiz",
		type : 'POST',
		data : JSON.stringify(json),
		contentType : "application/json",
		success : function(user) {
			
		}
	});
}

function getQuizResults() {
	var userQuestions = [];
	for ( var i in questions) {
		var question = {
				id : questions[i].id,
				answers : []
		};
		$.each($('input[question="' + questions[i].id + '"]'), function(i) {
			var answer = {
				id : $(this).attr("answer"),
				checked : $(this).is(":checked")
			};
			question.answers.push(answer);
		});
		userQuestions.push(question);
	}
	return userQuestions;
}

function getTotalResults() {
	var result = questions.length;
	for ( var index in questions) {
		var question = questions[index];
		var answers = question.answers;
		var isCorrect = true;
		for ( var a in answers) {
			var answer = answers[a];
			var checked = $('input[question="' + question.id + '"][answer="' + answer.id + '"]').is(":checked");
			if ((checked && !answer.correct) || (!checked && answer.correct))
				isCorrect = false;
		}
		if (!isCorrect)
			--result;
	}
	return result;
}

$(components.closeResultId).click(function() {
	location.reload();
});
